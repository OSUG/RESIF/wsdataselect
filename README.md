# Wsdataselect

Wsdataselect is an implementation of the dataselect web service as specified by the FDSN (https://www.fdsn.org/webservices/) to distribute seismological data.

In it's inners, the implementation takes in account all the context of the EPOS-France national seismological datacenter (https://seismology.resif.fr).
- database schema for the inventory (`Wsdataselect.Inventory`)
- detabase schema for authentication (`Wsdataselect.Auth`)
- message forward to the inner statistics system (`Wsdataselect.Statistics`)

## Chosen Technologies

This program is written un Elixir, with the following libraries:
- HTTP server Bandit https://github.com/mtrudel/bandit
- Ecto for interaction with the postgresql database https://github.com/elixir-ecto/ecto
- Plug for HTTP requests management https://hexdocs.pm/plug
- Sentry to catch exceptions and performance issues https://www.sentry.io/

## How it works

Requests are managed with Plug in the module `Wsdataselect.Router`.
This is the entry point.

The router then launches all the buiseness logic managed in the module `Wsdataselect.Controller`.

- Authentication if necessary
- Parsing of the requests 
- Validation of the request with the module `Wsdataselect.DataRequest`

Then, `Wsdataselect.Backend` is used to:
- fetch in the inventory all the data files that hold data requested by the user
- open the files in the archive and select the relevant data
- transmits the data to the client 
- transmits informations for data usage statistics `Wsdataselect.Statistics`


Each module has its own documentation giving all the details on how they work.
  
### Router
The router is very basic, handling the following endpoints:

- GET /query
- GET /queryauth
- POST /query
- POST /queryauth
- GET /health
- And some static files (user documentation and WADL description (`/application.wadl`)

### Authentication
`fdsnws-dataselect-1` describes authentication by HTTP Digest (https://datatracker.ietf.org/doc/html/rfc2617).

`Wsdataselect.Auth` impléments this protocol and triggered by `Wsdataselect.Controller.authenticate/1`.

Then, the user name (or `anonymous` if unknown) is added in the data request structure. 

### Parameters parsing and analyze
For a GET request, the function `Wsdataselect.HttpParameters.normalize/1` is mapping all the parameters in the URI to a `%Wsdataselect.DataRequest{}` structure.

For a POST request, the `Wsdataselect.BodyParser` implements the `Plug.Parser` behavior. Function `Wsdataselect.BodyParser.parse/5` is called automatically when a POST request arrives.

### Request validation
At this stage, the controler knwos all the parameters as submitted by the user. They have to be validated:

`Wsdataselect.DataRequest.fdsn_validate/1` makes the following validation steps:
- value for the nodata parameter (204 or 404)
- quality code is consistent with the sepecification
- validate each stream
- analyze start en end date parameters


From now on, `Wsdataselect.Controller.manage_query/2` is the common function to process a query (wether it is POST or GET).

### Jokers translation

Jokers caracters from the FDSN sepcification are translated to SQL jokers by `Wsdataselect.Stream.translate_wildcards/1`.

### Priviledges checks

`Wsdataselect.Backend.filter_permissions/1` excludes the stream for which user has not sufficient privileges.

### Data volume evaluation

In order not to start serving too large data requests, the function `Wsdataselect.Backend.evaluate_samples/1` is evaluating how many data the request is going to stream.

This is done by querying database for each stream and compute the number of samples by multiplying the time windows and the samplerate.

As soon as the total gets larger than the defined limit (see `MAXSAMPLES` environment variable), the client gets a "Too much data" response.

If the evaluation result is 0, then the client gets a "No data" response.

### Fetching and streaming data
`Wsdataselect.Backend.dataselect/2` manages data fetching and streaming in the following steps.

#### Files list
`Wsdataselect.Backend.files_in_archive/1` will retrieve the list of files from the inventory.

This list is filtered by the actual existence of the data file in the inventory.

If the resulting list is empty, "no data" response is sent to the user. Error messages in the logs are there for the operator to check out this inconsistency. 

#### Run dataselect
As Elixir has no library for miniSEED data format, we rely on the `dataselect` binary `dataselect` (https://github.com/EarthScope/dataselect/). 

The data is retrieved by chunks. For each chunk:
- write it down in a local file
- send the chunk to the client

#### Data usage statistics
A message is sent to a redis queue containing all the metadata of the request, with allows the statistics system to analyze the data and register the event in a local database.

## Deploy

### Prerequisites
- Inventory database on a postgresql server
- Authentication database also hosted on the same server, accessible to the same database user
- A REDIS servir used for authentication and statistics forward
- Data archive for validated seismological data, organized on daily files, by network code: `NET/YEAR/STA/CHA.D/`
- Raw data archive organized in daily files, by year `YEAR/NET/STA/CHA.D/`
- dataselect binary compiled and present in the application's PATH (see https://github.com/EarthScope/dataselect/)

### Configuration

Configuration is done with environmental variables, at runtime.

```
| DBUSER             | wsdataselect | User fro the SGBD 
| DBPASS             | wsdataselect | Password for SGBD
| DBHOST             | localhost    | SGBD host
| DBPORT             | 5432         | SGBD port
| INVDB              | resifInv     | Inventory database name
| AUTHDB             | resifAuth    | Authentication database name
| MAXSAMPLES         | 2000000000   | Maximum number of samples that can be streamed by the service 
| DATASELECT_TIMEOUT | 30000        | Timeout for dataselect binary to retrieve data from archive 
| POST_MAX_LINES     | 100          | Maximum number of lines for a POST data
| VALID_DATA_DIR     | /data        | Path to the validated data archive
| BUD_DATA_DIR       | /bud         | Path to the raw data archive
| REDIS_HOST         | localhost    | REDIS server
| REDIS_QUEUE        | fdsnwsstat   | REDIS queue for statistics
| WORKDIR            | workdir      | Working directory
```


#### About HTTP Digest Realm

The realm's value is by default "FDSN" which is hardcoded in the credential's hash at RESIF.

It can be changed at compilation time like this:

    REALM="MyRealm" mix compile
    
### Container

Pre-built containers are available in the Gricad Gitlab forge: https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/wsdataselect/container_registry/931

### Compilation and launch locally

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/wsdataselect.git
    cd wsdataselect
    mix deps.get
    mix run --nohalt
