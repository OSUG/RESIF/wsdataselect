defmodule Wsdataselect.Router do
  @moduledoc """
  This module is the entrypoint of HTT requests from the user. It uses Plug to manage the requests.
  """
  require Logger
  require DateTime
  alias Wsdataselect.Controller
  use Plug.Router

  plug(Plug.Logger)

  plug(Plug.Static,
    at: "/",
    from: {:wsdataselect, "priv/static"},
    content_types: %{"application.wadl" => "text/xml; charset=utf-8", "index.html" => "text/html"}
  )

  plug(Plug.RequestId)
  plug(:match)

  plug(Plug.Parsers,
    parsers: [Wsdataselect.BodyParser],
    pass: ["text/*"]
  )

  plug(Sentry.PlugContext, body_scrubber: &MySentryScrubber.scrub_params/1)
  plug(:dispatch)

  @impl Plug.ErrorHandler
  def handle_errors(conn, %{kind: _kind, reason: reason, stack: _stack}) do
    Logger.error("Internal error #{inspect(reason)}")
    Logger.debug("#{inspect(conn)}")
    send_resp(conn, conn.status, "Something went wrong")
  end

  # Health method checks the connection to the database and sned a 200 response code if it works.
  get "/health" do
    case Controller.health() do
      {:ok, reason} -> send_resp(conn, 200, reason)
      {:error, m} -> send_resp(conn, 500, "Bad health: #{m}")
    end
  end

  # When user hits the root of the webservice, we send back the documentation.
  get "/" do
    Controller.send_doc(conn)
  end

  post "/" do
    Controller.send_doc(conn)
  end

  get "/query" do
    # Parse query parameters and put them in conn
    Controller.manage_query(conn, "anonymous")
  end

  post "/query" do
    Logger.debug("Body: #{inspect(conn.body_params)}")
    Controller.manage_query(conn, "anonymous")
  end

  get "/queryauth" do
    Logger.info("Get request with authentication with params: #{inspect(conn)}")
    Controller.authenticate(conn)
  end

  post "/queryauth" do
    Logger.info("Post request with authentication: #{inspect(conn)}")
    Controller.authenticate(conn)
  end

  head "/" do
    send_resp(conn, 200, "")
  end

  head "/query" do
    send_resp(conn, 200, "")
  end

  head "/queryauth" do
    send_resp(conn, 200, "")
  end

  match _ do
    send_resp(conn, 404, "Not found")
  end
end
