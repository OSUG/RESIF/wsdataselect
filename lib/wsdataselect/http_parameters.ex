require Logger

defmodule Wsdataselect.HttpParameters do
  @moduledoc """
  Parse and normalize parameters as given by user through GET requests.
  """

  @doc """
  Normalize parameters, set default values
  """
  @spec normalize(map) :: map
  def normalize(params) do
    Logger.debug("Need to normalize: #{inspect(params)}")

    params
    |> keys_to_atoms()
    |> shorten_params()
    |> default_values()
  end

  # From a map with strings as keys, return the same map but with keys as atoms.
  @spec keys_to_atoms(map) :: map
  defp keys_to_atoms(params) do
    Enum.into(params, Map.new(), fn {k, v} -> {String.to_atom(k), v} end)
  end

  # Transform the long styled keys from params into short keys
  # Example:
  # normalize_params(%{starttime: "2010-01-01"})
  # => {%{start: "2010-01-01"}}
  @spec shorten_params(map) :: map
  defp shorten_params(params) do
    short_keys = %{
      starttime: :start,
      endtime: :end,
      network: :net,
      station: :sta,
      location: :loc,
      channel: :cha
    }

    Enum.reduce(params, %{}, fn {key, value}, acc ->
      normalized_key = Map.get(short_keys, key, key)
      Map.put(acc, normalized_key, value)
    end)
  end

  # Add default values for missing parameters.
  @spec default_values(map) :: map
  defp default_values(params) do
    defaults = %{
      start: Date.utc_today() |> DateTime.new!(~T[00:00:00]),
      end: DateTime.utc_now(),
      net: "*",
      sta: "*",
      loc: "*",
      cha: "*",
      nodata: "204",
      quality: "B"
    }

    Map.merge(defaults, params)
  end
end
