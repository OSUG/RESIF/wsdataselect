defmodule Wsdataselect.Auth.Repo do
  alias Wsdataselect.Auth.WsUsers
  require Logger
  import Ecto.Query

  use Ecto.Repo,
    otp_app: :wsdataselect,
    adapter: Ecto.Adapters.Postgres,
    read_only: true

  def init(_type, config) do
    dbuser = Application.get_env(:wsdataselect, :dbuser)
    dbpass = Application.get_env(:wsdataselect, :dbpass)
    dbhost = Application.get_env(:wsdataselect, :dbhost)
    dbport = Application.get_env(:wsdataselect, :dbport)
    db = Application.get_env(:wsdataselect, :authdb)
    {:ok, Keyword.put(config, :url, "ecto://#{dbuser}:#{dbpass}@#{dbhost}:#{dbport}/#{db}")}
  end

  def health({:error, message}) do
    {:error, message}
  end

  def health({:ok, message}) do
    query = from(w in WsUsers, limit: 1)

    try do
      one(query)
      {:ok, message <> "\n✓ Auth database OK"}
    rescue
      e ->
        Logger.error(e)
        {:error, message <> "\n💀 Auth database not queriable"}
    end
  end
end
