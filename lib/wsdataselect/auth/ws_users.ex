defmodule Wsdataselect.Auth.WsUsers do
  @moduledoc """
  Schema for matching a user with it's credential.
  """
  use Ecto.Schema
  @primary_key false
  schema "dataselect" do
    field(:login, :string)
    field(:email, :string)
    field(:digest, :string)
  end
end
