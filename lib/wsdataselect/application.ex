defmodule Wsdataselect.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    :logger.add_handler(:my_sentry_handler, Sentry.LoggerHandler, %{
      config: %{metadata: [:file, :line]}
    })

    children = [
      {Bandit, plug: Wsdataselect.Router},
      Wsdataselect.Inventory.Repo,
      Wsdataselect.Auth.Repo,
      {Redix, host: Application.get_env(:wsdataselect, :redis_host), name: :redix},
      {Cachex, name: :network}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Wsdataselect.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
