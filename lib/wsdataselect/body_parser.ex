require Logger
require Plug.Parsers.ParseError

defmodule Wsdataselect.BodyParser.ParseError do
  defexception message: "Malformed body"
end

defmodule Wsdataselect.BodyParser do
  @moduledoc """
  This modules parses the body of a HTTP request to generate a %Wsdataselect.DataRequest{} structure.

  The max number of lines in a post request can be parametrised using the environment variable POST_MAX_LINES.
  """
  alias Wsdataselect.{DataRequest, Stream}
  @behaviour Plug.Parsers

  def init(opts), do: opts

  @doc """
  Parse the body of the request in order to return a %Wsdataselect.DataRequest{}
  This function implements the behavior of Plug.Parsers.
  """
  @spec parse(Plug.Conn.t(), any, any, any, any) ::
          {:ok, DataRequest.t(), Plug.Conn.t()}
          | {:error, :too_large, Plug.Conn.t()}
          | {:next, Plug.Conn.t()}
  def parse(conn, _, _, _, _) do
    {:ok, body, conn} = Plug.Conn.read_body(conn)
    max_lines = Application.get_env(:wsdataselect, :post_max_lines)
    body_list = String.split(body, "\n", trim: true)

    if length(body_list) > max_lines do
      Logger.warning("Body has more than #{max_lines} lines. (#{length(body_list)})")
      {:error, :too_large, conn}
    else
      case Enum.reduce(body_list, %DataRequest{}, &parse_line/2) do
        {:error, m} ->
          Logger.error(m)
          Plug.Conn.send_resp(conn, 400, m)
          {:next, conn}

        datareq ->
          Logger.debug("Body parsed: #{inspect(datareq)}")
          {:ok, datareq, conn}
      end
    end
  end

  # Parse a body line and add the result to a %Wsdataselect.DataRequest{}
  # Returns %Datarequest{}
  @spec parse_line(String.t(), DataRequest.t() | {:error, String.t()}) ::
          {:error, String.t()} | DataRequest.t()
  defp parse_line(_, {:error, m}) do
    {:error, m}
  end

  defp parse_line(line, datareq) do
    param_line = ~r/^(?<key>.*)=(?<val>.*)$/

    case Regex.named_captures(param_line, line) do
      nil -> parse_stream(line, datareq)
      %{"key" => k, "val" => v} -> Map.replace(datareq, String.to_atom(k), v)
    end
  end

  @spec parse_stream(String.t(), DataRequest.t() | {:error, String.t()}) ::
          {:error, String.t()} | DataRequest.t()
  defp parse_stream(_, {:error, m}) do
    {:error, m}
  end

  defp parse_stream(line, datareq) do
    stream =
      line
      |> String.trim()
      |> String.split(~r/[[:space:]]+/)

    if Enum.count(stream) == 6 do
      Logger.debug("Stream: #{inspect(stream)}")

      stream_params =
        struct(Stream, %{
          net: Enum.at(stream, 0),
          sta: Enum.at(stream, 1),
          loc: Enum.at(stream, 2),
          cha: Enum.at(stream, 3),
          start: Enum.at(stream, 4),
          end: Enum.at(stream, 5)
        })

      Logger.debug("Stream: #{inspect(stream_params)}")
      streams = [stream_params | datareq.streams]
      Map.replace(datareq, :streams, streams)
    else
      {:error, "Malformed line in body: #{inspect(line)}"}
    end
  end
end
