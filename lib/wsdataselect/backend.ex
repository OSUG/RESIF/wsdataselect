defmodule Wsdataselect.Backend do
  @moduledoc """
  Implements the interaction with external resources like the inventory database or the archives.
  """
  require Logger
  require File
  require DateTime
  require Cachex
  alias Wsdataselect.{DataRequest, Stream, Archive, Parallel}

  alias Wsdataselect.Inventory.{
    Repo,
    Channel,
    Station,
    Network,
    Samplerate,
    ValidInventory,
    BudInventory,
    AutUser
  }

  use Timex
  import Ecto.Query

  @doc """
  Evaluate how much samples the request would generate.
  This is done by checking the samplerate of each channel and multiply by the number of seconds of the request.
  In the same time, the function adjusts the time selections to match only the epochs defined in the inventory
  """
  @spec evaluate_samples(DataRequest.t() | {:error, String.t()}) ::
          DataRequest.t() | {:error | :toomuch, String.t()} | {:nodata, DataRequest.t()}
  def evaluate_samples({:error, reason}) do
    {:error, reason}
  end

  def evaluate_samples(datareq) do
    # We should not serve requests with more than an arbitrary number of streams.
    Logger.debug("The request has #{length(datareq.streams)} streams")

    if length(datareq.streams) > 1000 do
      {:toomuch,
       "Request has been expanded to more than 1000 streams. Avoid using wildcards in network selection."}
    else
      limit = Application.get_env(:wsdataselect, :maxsamples)

      samples =
        datareq.streams
        |> Parallel.pmap(&evaluate_sample(&1))
        |> Enum.reduce(0, fn x, acc -> x + acc end)

      Logger.info("#{samples} samples evaluated for the request. Limit is set at #{limit}")

      cond do
        samples < limit ->
          datareq

        samples >= limit ->
          {:toomuch,
           "Request evaluated at #{samples} samples which is more than the limit (#{limit}). Please split your request in smaller parts"}

        samples == 0 ->
          {:nodata, datareq}

        true ->
          datareq
      end
    end
  end

  # Extends the network code based on the short network name and a year.
  # Returns the extended network code
  #
  @spec extend_code(String.t(), integer) :: String.t()
  def extend_code(net, year) do
    if Regex.match?(~r/[XYZ0-9][A-Z0-9]/, net) do
      # Maybe we have this correspondance already in the cache ?
      case Cachex.get(:network, extnet_cache_key(net, year)) do
        {:ok, nil} ->
          extnet = get_extended_code(net, year)

          case Cachex.put(:network, extnet_cache_key(net, year), extnet) do
            {:ok, true} -> Logger.debug("Extended code added in cache")
            {:error, m} -> Logger.error("Writing extended code to cache failed: #{m}")
          end

          extnet

        # Cache hit, good !
        {:ok, extnet} ->
          extnet
      end
    else
      # If it's not a temporary network code, then there is nothing to do, return the permanent network code.
      net
    end
  end

  # Get extended code from database
  defp get_extended_code(net, year) do
    query =
      from(n in Network,
        where: n.network == ^net,
        where: n.start_year <= ^year,
        select: n.start_year,
        order_by: [desc: n.start_year],
        limit: 1
      )

    case Repo.one(query) do
      nil -> {:error, "Temporary network code #{net} does not exist in year #{year}"}
      y -> "#{net}#{y}"
    end
  end

  # Defines how a cache key is identified
  @spec extnet_cache_key(String.t(), String.t()) :: String.t()
  defp extnet_cache_key(n, y) do
    "#{n}::#{y}"
  end

  @spec evaluate_sample(Stream.t()) :: Integer
  defp evaluate_sample(stream) do
    # If limit is already passed, do not bother with the database backend
    duration = DateTime.diff(stream.end, stream.start, :second)
    Logger.debug("Evaluating #{inspect(stream)} having a #{duration}s span")

    query =
      from(c in Channel,
        join: s in Station,
        on: c.station_id == s.station_id,
        join: n in Network,
        on: s.network_id == n.network_id,
        join: sr in Samplerate,
        on: c.samplerate_id == sr.samplerate_id,
        where: like(n.network, ^stream.net),
        where: like(s.station, ^stream.sta),
        where: like(c.location, ^stream.loc),
        where: like(c.channel, ^stream.cha),
        where: c.starttime < ^stream.end,
        where: c.endtime > ^stream.start,
        select:
          fragment(
            "sum(? * extract('epoch' from  least(c0.endtime,?::timestamp) - greatest(c0.starttime,?::timestamp)))::bigint",
            sr.samplerate,
            ^stream.end,
            ^stream.start
          )
      )

    case Repo.all(query) do
      [nil] ->
        Logger.debug("No sample for this stream ")
        0

      [r] ->
        Logger.debug("Samples for #{inspect(stream)} : #{inspect(r)}")
        r

      x ->
        Logger.debug("Samples for #{inspect(stream)} : #{inspect(x)}")
        x
    end
  end

  # Returns a list of full path of the files in the archives matching the selection parameters
  # Parameters
  #  - datareq : a %DataRequest{} structure
  @spec files_in_archive(DataRequest.t()) :: list(String.t())
  defp files_in_archive(datareq) do
    datareq.streams
    |> Parallel.pmap(&get_files(&1, datareq.quality))
    |> List.flatten()
    |> Enum.filter(&Archive.exists?(&1))
  end

  @doc """
  Format the selections parameters from %DataRequest{} as a selection string suitable for the dataselect executable.
  See the manual of dataselect, section "Selection File"

  ## Parameters

    - datareq : a %DataRequest{} structure
  """
  @spec make_selection_input(DataRequest.t()) :: String.t()
  def make_selection_input(datareq) do
    datareq.streams
    |> Enum.map(fn s -> Map.put(s, :quality, datareq.quality) end)
    |> Enum.reduce("", fn x, acc -> acc <> format_selection_input_line(x) end)
  end

  @spec format_selection_input_line(map) :: String.t()
  defp format_selection_input_line(stream) do
    # If quality in stream is B, then we translate it to "*"
    # Transmitting all qualities is allright since a file only has one type of quality in the archive.
    "#{stream.net} #{stream.sta} #{stream.loc} #{stream.cha} #{String.replace(stream.quality, "B", "*")} #{format_datetime_for_selection_list(stream.start)} #{format_datetime_for_selection_list(stream.end)}\n"
    |> String.replace("_", "?")
    |> String.replace("%", "*")
  end

  @spec format_datetime_for_selection_list(DateTime.t()) ::
          {:ok, String.t()} | {:error, String.t()}
  defp format_datetime_for_selection_list(%DateTime{} = dt) do
    case Timex.format(dt, "%Y,%j,%H,%M,%S.%f", :strftime) do
      {:ok, s} -> s
      {:error, m} -> {:error, m}
    end
  end

  @doc """
  This function issues requests to the database to get a list of files from a Wsdataselect.Stream structure and a requested quality code.
  """
  @spec get_files(Stream.t(), String.t()) :: list(Archive.t())
  def get_files(stream, quality) do
    Logger.debug(
      "Finding files in inventory for #{inspect(stream)} with quality #{inspect(quality)}"
    )

    # Enlarge the time selection 1 minute before and 1 minute after
    shift_start = Timex.shift(stream.start, minutes: -1)
    shift_end = Timex.shift(stream.end, minutes: 1)
    # Define the basic query in the validated archive
    query_validated =
      from(v in ValidInventory,
        join: c in Channel,
        on: v.channel_id == c.channel_id,
        join: s in Station,
        on: c.station_id == s.station_id,
        join: n in Network,
        on: s.network_id == n.network_id,
        where: like(n.network, ^stream.net),
        where: like(s.station, ^stream.sta),
        where: like(c.location, ^stream.loc),
        where: like(c.channel, ^stream.cha),
        where: c.starttime < ^shift_end,
        where: c.endtime > ^shift_start,
        where: v.starttime < ^shift_end,
        where: v.endtime > ^shift_start,
        select: %{
          source_file: v.source_file,
          quality: v.quality,
          tsindex: v.timeindex,
          archive: "validated"
        }
      )

    # Define the basic query in the bud archive
    query_bud =
      from(v in BudInventory,
        join: c in Channel,
        on: v.channel_id == c.channel_id,
        join: s in Station,
        on: c.station_id == s.station_id,
        join: n in Network,
        on: s.network_id == n.network_id,
        where: like(n.network, ^stream.net),
        where: like(s.station, ^stream.sta),
        where: like(c.location, ^stream.loc),
        where: like(c.channel, ^stream.cha),
        where: c.starttime < ^shift_end,
        where: c.endtime > ^shift_start,
        where: v.starttime < ^shift_end,
        where: v.endtime > ^shift_start,
        select: %{
          source_file: v.source_file,
          quality: v.quality,
          tsindex: v.timeindex,
          archive: "bud"
        }
      )

    # Now, we want to adapt the query based on the quality parameter
    query =
      cond do
        # M and Q are only present in the validated archive, so we can pick only from there
        quality in ["M", "Q"] ->
          from(v in query_validated, where: v.quality == ^quality)

        # R and D are in both archives, we add the where clause and do a UNION of both queries
        quality in ["R", "D"] ->
          query_validated = from(v in query_validated, where: v.quality == ^quality)
          query_bud = from(v in query_bud, where: v.quality == ^quality)
          from(q in query_bud, union_all: ^query_validated)

        # B stands for "Best", there is no such quality code in miniSEED, this means we fetch every possible entry.
        # Filtering on best quality is done afterwards
        quality == "B" ->
          from(q in query_bud, union_all: ^query_validated)
      end

    # Sorting by source_file guarantees that data can be transmitted in a consistent order.
    # i.e. Channel by channel, respecting time order.
    Repo.all(query)
    |> Enum.map(fn x ->
      struct(Archive, %{
        source_file: x.source_file,
        quality: x.quality,
        archive: x.archive,
        tsindex: x.tsindex
      })
    end)
    # Resif-DC delivers only the best quality data.
    |> Archive.filter_on_quality()
    # Set the full path to the file
    |> Enum.map(&Archive.expand_path(&1))
    # Set start offset on each file
    |> Enum.map(&Archive.set_offset(&1, shift_start, :before))
    # Set the end offset
    |> Enum.map(&Archive.set_offset(&1, shift_end, :after))
  end

  @doc """
  Filters all streams based on user permission
  Keep only streams for open networks and closed networks where the user is authorized

  Returns `{:ok, %Wsdataselect.DataRequest{}}`

  # Parameters

  - `{:ok, %Wsdataselect.DataRequest{}}`
  - `{:error, message}`
  """
  @spec filter_permissions(DataRequest.t() | {:error, String.t()}) ::
          DataRequest.t() | {:error, String.t()}
  def filter_permissions({:error, m}) do
    {:error, m}
  end

  def filter_permissions(%DataRequest{} = datareq) do
    Logger.debug(
      "Checking permissions. Remove requested streams where user #{datareq.user} has no access grant."
    )

    filtered_streams =
      datareq.streams
      |> Enum.flat_map(&Stream.expand_on_network(&1))
      |> Enum.filter(&stream_allowed?(&1, datareq.user))

    %{datareq | streams: filtered_streams}
  end

  @spec stream_allowed?(Stream.t(), String.t()) :: boolean()
  def stream_allowed?(str, user) do
    Logger.debug("Check if #{user} can access to stream #{inspect(str)}")
    # Get network for this stream and test each of them
    case Cachex.fetch(:network, "#{str.net_id}::#{user}", &stream_allowed_db_fallback/1) do
      {:ok, b} ->
        b

      {:commit, b} ->
        b

      {:error, m} ->
        Logger.error("Problem while getting #{str.net_id}::#{user} from cache: #{m}")
        # Maybe raise something here ?
        false
    end
  end

  @spec stream_allowed_db_fallback(String.t()) :: boolean()
  defp stream_allowed_db_fallback(key) do
    [netid, user] = String.split(key, "::")
    Logger.debug("Cache miss, fetching network policy from database")
    query = from(n in Network, where: n.network_id == ^netid)

    case Repo.one(query) do
      # Si le réseau est inconnu, autant l'éliminer tout de suite de la requête
      nil -> false
      %{policy: "open"} -> true
      %{policy: "closed", network_id: network_id} -> network_allowed?(network_id, user)
      _ -> false
    end
  end

  @spec network_allowed?(integer, String.t()) :: boolean
  defp network_allowed?(net, user) do
    Logger.debug("Check if #{user} can access to network id #{net}")

    case Repo.get_by(AutUser, network_id: net, name: user) do
      nil ->
        Logger.info("User #{user} is not allowed to access network id #{net}")
        false

      _ ->
        true
    end
  end

  @doc """
  Modify the %DataRequest{} struct by adding the :file attribute and a list of files to retrieve
  """
  @spec get_data_files(
          {:error | :toomuch, String.t()}
          | {:nodata, DataRequest.t()}
          | DataRequest.t()
        ) :: DataRequest.t() | {:error | :toomuch, String.t()} | {:nodata, DataRequest.t()}
  def get_data_files({:error, msg}) do
    {:error, msg}
  end

  def get_data_files({:toomuch, msg}) do
    {:toomuch, msg}
  end

  def get_data_files({:nodata, datareq}) do
    {:nodata, datareq}
  end

  def get_data_files(%DataRequest{} = datareq) do
    files = files_in_archive(datareq)

    case length(files) do
      0 -> {:nodata, datareq}
      _ -> %{datareq | files: files}
    end
  end

  @doc """
  This function prepares the temporary directory which will contain :
  - the matching criteria written in a matchfile (match)
  The directory is later used by the Controller to launch the dataselect binary and stream data to the clients.
  The retrieved data is then written in that directory.
  The directory is then accessed by the statistics program for analyse and cleaned up.
  """
  @spec prepare_workdir(
          {:error | :toomuch, String.t()}
          | {:nodata, DataRequest.t()}
          | DataRequest.t()
        ) ::
          DataRequest.t()
          | {:error | :eintern | :toomuch, String.t()}
          | {:nodata, DataRequest.t()}
  def prepare_workdir({:error, msg}) do
    {:error, msg}
  end

  def prepare_workdir({:toomuch, msg}) do
    {:toomuch, msg}
  end

  def prepare_workdir({:nodata, datareq}) do
    {:nodata, datareq}
  end

  def prepare_workdir(datareq) do
    workdir = get_workdir(short_request_id())

    with :ok <- File.mkdir(workdir),
         # :ok <- write_files_list(datareq, workdir),
         :ok <- write_match_file(datareq, workdir) do
      datareq
    else
      {:error, x} -> {:eintern, "Unable to write to #{workdir}. #{x}"}
    end
  end

  @spec write_match_file(DataRequest.t(), String.t()) :: :ok | {:error, String.t()}
  defp write_match_file(datareq, workdir) do
    match_file = "#{workdir}/match"

    match_content =
      datareq.streams
      |> Enum.map(fn s -> Map.put(s, :quality, datareq.quality) end)
      |> Enum.reduce("", fn x, acc -> acc <> format_selection_input_line(x) end)

    File.write(match_file, match_content)
  end

  @doc """
  This function takes a list of Archive.
  It actually reads data from archive and returns the data as a list, using dataselect binary.
  It expects a match file to be available (previously prepared by write_match_file/2)
  It returns the list of absolute paths of data files collected.
  """
  @spec read_all_data(list(Archive.t())) :: {:ok, String.t()} | {:error, String.t()}
  def read_all_data(files) when is_list(files) do
    Logger.info("Starting read of data using dataselect binary")
    dataselect_concurrency = Application.get_env(:wsdataselect, :dataselect_concurrency)
    workdir = get_workdir(short_request_id())
    File.mkdir("#{workdir}/delivered")

    {time_in_microseconds, results} =
      :timer.tc(fn ->
        Parallel.pmap_chunk(files, dataselect_concurrency, &read_file(&1, workdir))
      end)

    if Enum.any?(results, fn x -> x != :ok end) do
      {:error, "Errors while reading the data."}
    else
      # HERE log the time spent on reading data
      # We should also compute the throughput ?
      data_size = directory_size(workdir <> "/delivered")

      if time_in_microseconds > 0 do
        Logger.info(
          "End read of data to workdir. {size: #{data_size}, duration: #{time_in_microseconds / 1_000}, throughput: #{trunc(data_size / time_in_microseconds * 1_000_000)}}"
        )
      end

      {:ok, workdir <> "/delivered"}
    end
  end

  @spec read_file(Archive.t(), String.t()) :: :ok | :timeout
  def read_file(%Archive{} = file, workdir) do
    dataselect_bin = Application.get_env(:wsdataselect, :dataselect)

    Logger.debug(
      "Running dataselect on #{file.path} into #{workdir}/delivered/#{file.source_file}"
    )

    Port.open(
      {:spawn_executable, dataselect_bin},
      [
        :binary,
        :exit_status,
        args: [
          "-Ps",
          "-s",
          "#{workdir}/match",
          "-o",
          "#{workdir}/delivered/#{file.source_file}",
          Archive.path_with_offset(file)
        ]
      ]
    )
    |> wait_for_completion()
  end

  defp directory_size(dir) do
    case File.ls(dir) do
      {:ok, files} ->
        Enum.reduce(files, 0, fn f, acc ->
          acc + (File.stat!("#{dir}/#{f}") |> Map.get(:size))
        end)

      _ ->
        0
    end
  end

  @spec wait_for_completion(port) :: :ok | :timeout
  defp wait_for_completion(port) do
    receive do
      {^port, {:data, _}} ->
        Logger.debug("Still fetching data")
        :ok

      {^port, {:exit_status, status}} ->
        Logger.debug("Dataselect finished with status #{status}")
        :ok

      _other ->
        wait_for_completion(port)
    after
      Application.get_env(:wsdataselect, :dataselect_timeout) ->
        Logger.info("Timeout on dataselect binary")
        :timeout
    end
  end

  # Prepare metadata en send statistics
  @spec send_statistics(Plug.Conn.t(), DataRequest.t(), String.t()) :: atom
  def send_statistics(conn, datareq, reqid) do
    Logger.metadata([{:request_id, reqid}])
    reqid = short_request_id()
    workdir = get_workdir(reqid)
    # Get missing metadata
    req_headers = Enum.into(conn.req_headers, %{})
    clientip = get_client_ip(conn)
    useragent = req_headers["user-agent"]

    hostname =
      case :inet.gethostname() do
        {:ok, h} -> to_string(h)
      end

    # Count files in all files list files
    nbfiles = length(datareq.files)

    case File.ls("#{workdir}/delivered") do
      {:ok, files} ->
        # This is a hack to be compatible with fdsnwsstats
        # Write the data to delivered.seed and delete the original file.
        delivered = File.open!("#{workdir}/delivered.seed", [:write])

        Enum.map(files, fn f -> "#{workdir}/delivered/#{f}" end)
        |> Enum.each(fn f ->
          data = File.read!(f)
          IO.binwrite(delivered, data)
          File.rm!(f)
        end)

      _ ->
        Logger.info("Registering statistics for no data")
    end

    case File.stat("#{workdir}/delivered.seed") do
      {:ok, fstat} -> Logger.info("Registering statistics for #{fstat.size} bytes of data")
      {:error, _} -> Logger.info("Registering statistics for no data")
    end

    # Send a message to the redis queue for the statistics program to consume
    redisq = Application.get_env(:wsdataselect, :redis_stats_queue)

    message =
      Jason.encode!(%{
        timestamp: Timex.now("Europe/Paris"),
        ws: "dataselect",
        hostname: hostname,
        user: datareq.user,
        requestid: reqid,
        useragent: useragent,
        clientip: clientip,
        nbfiles: nbfiles,
        matchlist: make_selection_input(datareq)
      })

    case Redix.command(:redix, ["RPUSH", redisq, message]) do
      {:ok, _} ->
        Logger.info("Message for statistics analyze sent to queue #{redisq}")

      {:error, m} ->
        Logger.warning("Error sending message to redis for statistics. #{inspect(m)}")
    end

    :ok
  end

  @spec get_client_ip(Plug.Conn.t()) :: String.t()
  defp get_client_ip(conn) do
    req_headers = Enum.into(conn.req_headers, %{})

    cond do
      Map.has_key?(req_headers, "x-forwarded-for") -> req_headers["x-forwarded-for"]
      Map.has_key?(conn, :remote_ip) -> Tuple.to_list(conn.remote_ip) |> Enum.join(".")
      true -> "127.0.0.1"
    end
  end

  @spec get_workdir(String.t()) :: String.t()
  defp get_workdir(reqid) do
    "#{Application.get_env(:wsdataselect, :workdir)}/#{reqid}"
  end

  # Use the requestLogger.metadata
  @spec short_request_id() :: String.t()
  def short_request_id do
    Logger.debug("Computing request_id with #{inspect(Logger.metadata())}")

    case Logger.metadata()[:request_id] do
      nil -> String.duplicate("0", 12)
      id -> String.slice(id, 0..11)
    end
  end
end
