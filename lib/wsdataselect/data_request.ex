defmodule Wsdataselect.DataRequest do
  require Logger

  alias Wsdataselect.Stream

  @moduledoc """
  This module defines a structure to represent a data request and functions to validate the parameters given by the user.
  """

  defstruct quality: "B",
            nodata: "204",
            user: "anonymous",
            streams: [],
            files: []

  @typedoc """
  A data request contains all parameters from the user's query and a list of streams. It also contains some metadata from the request.
  """
  @type t() :: %Wsdataselect.DataRequest{
          quality: String.t(),
          nodata: String.t() | integer,
          user: String.t(),
          streams: list(%Stream{}),
          files: list()
        }

  @spec expand_streams(t() | {:error, String.t()}) :: t() | {:error, String.t()}
  def expand_streams({:error, msg}) do
    {:error, msg}
  end

  def expand_streams(datareq) do
    Logger.debug("Expanding streams when parameters have coma separated values")
    streams = Enum.flat_map(datareq.streams, &Stream.expand_multi_values/1)
    Logger.debug("Streams expanded: #{inspect(streams)}")
    %{datareq | streams: streams}
  end

  @doc """
  For each stream in the request, intersect with metadata inventory.
  """
  @spec match_streams_with_inventory(t() | {:error, String.t()}) :: t() | {:error, String.t()}
  def match_streams_with_inventory({:error, msg}) do
    {:error, msg}
  end

  def match_streams_with_inventory(datareq) do
    Logger.debug("Expanding streams having wildcards and match datetimes to inventory")
    streams = Enum.flat_map(datareq.streams, &Stream.match_with_inventory/1)
    Logger.debug("Streams expanded: #{inspect(streams)}")
    %{datareq | streams: streams}
  end

  @doc """
  Validates all request parameters, sets default values according to fdsnws-dataselect-1 specification.
  Also transform dates to %DateTime{} structures
  Returns a tuple: {:ok, %WsDatarequest{}} or {:error, message}
  """
  @spec fdsn_validate(t()) :: t() | {:error, String.t()}
  def fdsn_validate(%Wsdataselect.DataRequest{} = datareq) do
    Logger.debug(
      "Validate that parameters and stream selection fo conform to the FDSN specification"
    )

    # {:ok, Map.replace(datareq, :id, generate_transaction_id())}
    datareq
    |> validate_nodata()
    |> validate_quality()
    |> validate_minimal_selection()
    |> validate_streams()
    |> parse_streams_dates()
  end

  @spec validate_minimal_selection(t()) :: t() | {:error, String.t()}
  defp validate_minimal_selection({:error, msg}) do
    {:error, msg}
  end

  defp validate_minimal_selection(datareq) do
    if Enum.any?(datareq.streams, fn s ->
         s.net == "*" and s.sta == "*" and s.loc == "*" and s.cha == "*"
       end) do
      {:error, "At least one of network, station, location or channel must be specified"}
    else
      datareq
    end
  end

  # Checks if the value for nodata is valid
  # Updates nodata value, casted to Integer
  @spec validate_nodata(t()) :: t() | {:error, String.t()}
  defp validate_nodata(params) do
    Logger.debug("Entering validate_nodata")

    case nodata_valid?(params.nodata) do
      204 -> %{params | nodata: 204}
      404 -> %{params | nodata: 404}
      {:error, reason} -> {:error, reason}
    end
  end

  @spec nodata_valid?(String.t()) :: integer | {:error, String.t()}
  defp nodata_valid?("204"), do: 204
  defp nodata_valid?("404"), do: 404

  defp nodata_valid?(v),
    do: {:error, "nodata parameter can only be 204 or 404 (was #{inspect(v)})"}

  # Validates that the quality character is correct.
  @spec validate_quality(t() | {:error, String.t()}) :: t() | {:error, String.t()}
  defp validate_quality({:error, reason}) do
    {:error, reason}
  end

  defp validate_quality(params) do
    Logger.debug("Entering validate_quality")

    if Enum.member?(["D", "R", "Q", "M", "B"], params.quality) do
      params
    else
      {:error, "Quality #{params.quality} not valid."}
    end
  end

  # Check that all streams are correct.
  # Actually, this function works for an arbitrary number of streams, but in practice, there will be only one since
  # it is called only for GET requests.
  @spec validate_streams(t() | {:error, String.t()}) :: t() | {:error, String.t()}
  defp validate_streams({:error, reason}) do
    {:error, reason}
  end

  defp validate_streams(datareq) do
    # L'inconvénient de cette méthode, c'est qu'on perd le stream qui a causé le problème de validation.
    Logger.debug("Validating all streams #{inspect(datareq.streams)}")

    Enum.find_value(datareq.streams, datareq, fn s ->
      case Stream.validate(s) do
        {:ok, _} -> false
        {:error, msg} -> {:error, msg}
      end
    end)
  end

  @spec parse_streams_dates(t() | {:error, t()}) :: t() | {:error, String.t()}
  defp parse_streams_dates({:error, msg}) do
    {:error, msg}
  end

  defp parse_streams_dates(datareq) do
    Logger.debug("Parse dates (already validated before): #{inspect(datareq)}")
    streams = Enum.map(datareq.streams, &Stream.parse_dates(&1))
    # If any stream has an error, return the error
    case Enum.find(streams, fn x -> !is_struct(x, Stream) end) do
      nil -> %{datareq | streams: streams}
      x -> x
    end
  end
end

# defimpl Enumerable, for: Wsdataselect.DataRequest do
#   def
# end
