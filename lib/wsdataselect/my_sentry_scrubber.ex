require Sentry

defmodule MySentryScrubber do
  @moduledoc """
  Add Plug parameters to the metadata sent to Sentry
  """
  def scrub_params(conn) do
    case is_struct(conn.params) do
      true ->
        %{conn | params: Map.from_struct(conn.params)}
        |> Sentry.PlugContext.default_body_scrubber()

      false ->
        Sentry.PlugContext.default_body_scrubber(conn)
    end
  end
end
