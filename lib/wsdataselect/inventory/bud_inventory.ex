defmodule Wsdataselect.Inventory.BudInventory do
  @moduledoc """
  Schema for the inventory of bud files.
  """
  alias Wsdataselect.Inventory.Channel
  use Ecto.Schema

  @primary_key {:rbud_id, :id, autogenerate: false}
  schema "rbud" do
    belongs_to(:channel, Channel, references: :channel_id)
    field(:source_file, :string)
    field(:xxh64, :string)
    field(:quality, :string)
    field(:starttime, :utc_datetime)
    field(:endtime, :utc_datetime)
    field(:timeindex, {:array, :map}, default: [])
  end
end
