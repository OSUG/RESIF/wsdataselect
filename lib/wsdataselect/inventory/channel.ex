defmodule Wsdataselect.Inventory.Channel do
  @moduledoc """
  Schema of the channels
  """
  use Ecto.Schema
  alias Wsdataselect.Inventory.{Station, Samplerate, InfinityUtcDatetimeType}

  @primary_key {:channel_id, :id, autogenerate: false}
  schema "channel" do
    belongs_to(:station, Station, references: :station_id)
    belongs_to(:samplerate, Samplerate, references: :samplerate_id)
    field(:location, :string)
    field(:channel, :string)
    field(:starttime, InfinityUtcDatetimeType)
    field(:endtime, InfinityUtcDatetimeType)
    field(:policy, :string)
  end
end
