defmodule Wsdataselect.Inventory.Repo do
  @moduledoc """
  Module to interact with the inventory database
  """
  require Logger
  import Ecto.Query

  use Ecto.Repo,
    otp_app: :wsdataselect,
    adapter: Ecto.Adapters.Postgres,
    read_only: true

  def init(_type, config) do
    dbuser = Application.get_env(:wsdataselect, :dbuser)
    dbpass = Application.get_env(:wsdataselect, :dbpass)
    dbhost = Application.get_env(:wsdataselect, :dbhost)
    dbport = Application.get_env(:wsdataselect, :dbport)
    db = Application.get_env(:wsdataselect, :invdb)
    {:ok, Keyword.put(config, :url, "ecto://#{dbuser}:#{dbpass}@#{dbhost}:#{dbport}/#{db}")}
  end

  def health({:error, message}) do
    {:error, message}
  end

  def health({:ok, message}) do
    query = from(n in Wsdataselect.Inventory.Network, limit: 1)

    try do
      one(query)
      {:ok, message <> "\n✓ Inventory repo OK"}
    rescue
      e ->
        Logger.error(e)
        {:error, message <> "\n💀 Inventory repo not queriable"}
    end
  end
end
