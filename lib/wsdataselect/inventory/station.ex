defmodule Wsdataselect.Inventory.Station do
  @moduledoc """
  Schema for the database table of the stations
  """
  alias Wsdataselect.Inventory.{Network, Channel}
  use Ecto.Schema

  @primary_key {:station_id, :id, autogenerate: true}
  schema "station" do
    field(:station, :string)
    belongs_to(:network, Network, references: :network_id)
    has_many(:channel, Channel, foreign_key: :station_id)
  end
end
