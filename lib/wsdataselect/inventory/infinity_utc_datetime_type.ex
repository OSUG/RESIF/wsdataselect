# Code généré avec ChatGPT pour gérer les infinis dans les dates
defmodule Wsdataselect.Inventory.InfinityUtcDatetimeType do
  @moduledoc """
  Custom Ecto type to manage infinity in the dates.
  """
  use Ecto.Type

  def type, do: :utc_datetime

  def cast(value) when value == :inf, do: {:ok, :inf}
  def cast(value) when value == "-inf", do: {:ok, :"-inf"}
  def cast(value), do: Ecto.Type.cast(:utc_datetime, value)

  def load(:inf), do: {:ok, ~U[9999-12-31 23:59:59Z]}
  def load(:"-inf"), do: {:ok, ~U[-9999-12-31 23:59:59Z]}
  def load(value), do: Ecto.Type.load(:utc_datetime, value)

  def dump(~U[9999-12-31 23:59:59Z]), do: {:ok, :inf}
  def dump(~U[-9999-12-31 23:59:59Z]), do: {:ok, :"-inf"}
  def dump(value), do: Ecto.Type.dump(:utc_datetime, value)
end
