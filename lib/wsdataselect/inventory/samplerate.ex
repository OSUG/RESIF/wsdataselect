defmodule Wsdataselect.Inventory.Samplerate do
  @moduledoc """
  Schema of the table of sample rates
  """
  use Ecto.Schema

  @primary_key {:samplerate_id, :id, autogenerate: false}
  schema "samplerate" do
    field(:samplerate, :float)
  end
end
