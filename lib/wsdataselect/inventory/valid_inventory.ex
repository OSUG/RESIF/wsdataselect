defmodule Wsdataselect.Inventory.ValidInventory do
  @moduledoc """
  Schema of the table for the inventory of all validated files in the archive.
  """
  alias Wsdataselect.Inventory.Channel
  use Ecto.Schema

  @primary_key {:rall_id, :id, autogenerate: false}
  schema "rall" do
    belongs_to(:channel, Channel, references: :channel_id)
    field(:source_file, :string)
    field(:xxh64, :string)
    field(:quality, :string)
    field(:starttime, :utc_datetime)
    field(:endtime, :utc_datetime)
    field(:timeindex, {:array, :map}, default: [])
  end
end
