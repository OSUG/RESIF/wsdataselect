defmodule Wsdataselect.Inventory.AutUser do
  @moduledoc """
  Schema mapping a network identifier to a user name in order to check permissions
  """
  use Ecto.Schema

  @primary_key false
  schema "aut_user" do
    field(:network_id, :integer)
    field(:name, :string)
  end
end
