defmodule Wsdataselect.Inventory.Network do
  @moduledoc """
  Schema of the table networks.
  """
  use Ecto.Schema
  alias Wsdataselect.Inventory.{Station, InfinityUtcDatetimeType}

  @primary_key {:network_id, :id, autogenerate: false}
  schema "networks" do
    field(:network, :string)
    field(:start_year, :integer)
    field(:starttime, InfinityUtcDatetimeType)
    field(:endtime, InfinityUtcDatetimeType)
    field(:policy, :string)
    has_many(:station, Station, foreign_key: :network_id)
  end
end
