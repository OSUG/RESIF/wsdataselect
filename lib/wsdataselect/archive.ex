defmodule Wsdataselect.Archive do
  require Logger
  require Path
  alias Wsdataselect.Backend

  @moduledoc """
  Module to manipulate files in the archive.
  """

  defstruct path: "",
            source_file: "",
            archive: "",
            quality: "",
            tsindex: [],
            start_offset: 0,
            end_offset: nil

  @typedoc """
  An archive structure.
  tsindex is a list of maps stored in json ie. [{"offset": 0, "timestamp": 1111}]
  """
  @type t() :: %Wsdataselect.Archive{
          path: String.t(),
          source_file: String.t(),
          quality: String.t(),
          archive: String.t(),
          tsindex: list,
          start_offset: integer(),
          end_offset: integer()
        }

  @spec exists?(t()) :: boolean
  def exists?(file) do
    case File.exists?(file.path) do
      true ->
        true

      false ->
        Logger.alert("File #{file.path} is missing in archive but present in inventory")
        false
    end
  end

  @spec expand_path(t()) :: t()
  def expand_path(archive) do
    Logger.debug("Expanding path #{inspect(archive)}")
    [net, sta, _, cha, _, y, _] = String.split(archive.source_file, ".")

    case archive.archive do
      # NET/YYYY/STA/CHA.D/file
      "validated" ->
        # Expand to validated repository
        case Integer.parse(y) do
          {year, _} ->
            net = Backend.extend_code(net, year)

            Map.replace(
              archive,
              :path,
              "#{Application.get_env(:wsdataselect, :valid_data_dir)}/#{net}/#{year}/#{sta}/#{cha}.D/#{archive.source_file}"
            )

          :error ->
            Logger.error(
              "Unexpected format of source file name: #{archive.source_file}. Could not guess year with #{y}. Try to continue anyway."
            )
        end

      # YYYY/NET/STA/CHA.D/file
      "bud" ->
        Map.replace(
          archive,
          :path,
          "#{Application.get_env(:wsdataselect, :bud_data_dir)}/#{y}/#{net}/#{sta}/#{cha}.D/#{archive.source_file}"
        )

      x ->
        Logger.error("Archive type #{x} undefined.")
        archive
    end
  end

  @doc """
  Find the best fitting offset.
  before_or_after is expected to be one of the following values:
  :start to get the best fitting offset before time
  :after to get the best fitting offset after time
  Timeindex is expected to be like:
  [ %{"offset" => 0, "timestamp" => 533174407868300000} ]

  """
  @spec set_offset(t(), DateTime.t(), atom()) :: t() | {:error, String.t()}
  def set_offset(%Wsdataselect.Archive{} = archive, time, before_or_after) do
    Logger.debug(
      "Finding the best offset at #{inspect(before_or_after)} of #{archive.source_file} at #{time}"
    )

    ts = DateTime.to_unix(time) * 10 ** 9
    # Bisect search to find the first index after time
    i =
      Bisect.search(
        archive.tsindex,
        fn x -> x > ts end,
        lhs_key: ["timestamp"]
      )

    case before_or_after do
      :before ->
        # If the index is 0 or 1, then the offset is 0.
        # Otherwise, we get the offset value from tsindex
        offset =
          if i > 1 do
            archive.tsindex |> Enum.at(i - 1) |> Map.get("offset")
          else
            0
          end

        Logger.debug("Start offset: #{offset}")
        %{archive | start_offset: offset}

      :after ->
        case archive.tsindex |> Enum.at(i) do
          nil ->
            %{archive | end_offset: nil}

          x ->
            offset = Map.get(x, "offset")
            Logger.debug("End offset: #{inspect(x)}")
            %{archive | end_offset: offset}
        end

      _ ->
        {:error, "Argument #{before_or_after} unknown. Must be :before or :after"}
    end
  end

  @doc """
  In a list of query results, if the file is the same, only return the one with highest quality

  ## Parameter

  - files: A list of %Wsdataselect.Archive{}

  ## Examples

  iex> Wsdataselect.Archive.filter_on_quality([
  ...>%{archive: "a", quality: "R", source_file: "a"},
  ...>%{archive: "a", quality: "D", source_file: "b"},
  ...>%{archive: "b", quality: "M", source_file: "a"},
  ...>%{archive: "b", quality: "Q", source_file: "b"}])

  [
  %{archive: "b", quality: "M", source_file: "a", offset: 0},
  %{archive: "b", quality: "Q", source_file: "b", offset: 0}
  ]
  """
  @spec filter_on_quality(list(t())) :: list(t())
  def filter_on_quality(files) do
    Logger.debug("Filtering #{inspect(files)} on quality")
    # By grouping on file name, we will have entries from all archives.
    Enum.group_by(files, &Map.fetch!(&1, :source_file))
    # For each element of the resulting Map, just keep the best of them.
    |> Enum.map(fn {_, v} -> keep_best_quality(v) end)
  end

  # Sort on quality, best quality first and return the head of the list.
  @spec keep_best_quality(list) :: map
  defp keep_best_quality(files) do
    Enum.sort(files, &comapre_quality(&1.quality, &2.quality)) |> hd()
  end

  # Compare two qualities
  defp comapre_quality(q1, q2) do
    get_quality_priority(q1) <= get_quality_priority(q2)
  end

  # Return a priority number from a quality letter
  # The lower, the best quality
  @quality_priorities %{"Q" => 0, "M" => 1, "D" => 2, "R" => 3}
  defp get_quality_priority(q) do
    case Map.fetch(@quality_priorities, q) do
      :error ->
        Logger.error(
          "Quality priority for #{q} is not defined in #{inspect(@quality_priorities)}"
        )

        99

      {:ok, v} ->
        v
    end
  end

  @spec path_with_offset(t()) :: String.t()
  def path_with_offset(%Wsdataselect.Archive{} = archive) do
    if archive.end_offset == nil do
      "#{archive.path}@#{archive.start_offset}"
    else
      "#{archive.path}@#{archive.start_offset}:#{archive.end_offset}"
    end
  end

  @doc """
  Tests the archive.
  # Parameters

  - `{:ok, message}`
  - `arch` : an atom indicating wich archive to look for. Valids are `:valid_data_dir` or `:bud_data_dir`

  # Return

  A tupple: `{:ok, message}` if file is accessible
  """
  @spec health({:error | :ok, String.t()}, atom) :: {:ok | :error, String.t()}
  def health({:error, msg}, _) do
    {:error, msg}
  end

  def health({:ok, msg}, arch) do
    dir = Application.get_env(:wsdataselect, arch)

    case File.ls(dir) do
      {:ok, _} -> {:ok, msg <> "\n✓ #{dir} archive"}
      {:error, m} -> {:error, msg <> "\n💀 #{dir} not accessible: #{m}"}
    end
  end
end
