defmodule Wsdataselect.Controller do
  @moduledoc """
  Controller for all incoming requests.
  The controller has all the logic of the web service in order to manage user's queries.
  """

  require Logger
  alias Wsdataselect.{DataRequest, Stream, HttpParameters, Backend, Auth, Inventory, Archive}

  @doc """
  Controller function to rollout all the authentication process.
  Sends response to the client according to authentication result
  Returns : Plug.Conn
  """
  @spec authenticate(Plug.Conn.t()) :: Plug.Conn.t()
  def authenticate(conn) do
    case Auth.authenticate(conn) do
      {:challenge_set, header_text} ->
        conn
        |> Plug.Conn.put_resp_header("WWW-Authenticate", header_text)
        |> Plug.Conn.send_resp(401, "Unauthorized")

      {:error, m} ->
        conn
        |> Plug.Conn.send_resp(401, m)

      {:ok, user} ->
        manage_query(conn, user)
    end
  end

  @doc """
  This is the entrypoint of the controller.
  Manages all the query parameters by doing:
  - FDSN validation to check if the query parameters make sense regarding the FDSN specification
  - Expand all wildcards in the parameters
  - Filter out streams that do not make any sense in the inventory
  - Translate wildcards from FDSN spec to SQL spec
  - Filter all the streams requested by checking user's permissions
  - Evaluate the number of samples the request could return
  - Prepare a working directory that contains everything to read the data user asked for
  """
  @spec manage_query(Plug.Conn.t(), String.t()) :: Plug.Conn.t()
  def manage_query(conn, user \\ "anonymous") do
    datareq =
      if is_struct(conn.body_params) do
        Logger.info("Serving POST query #{inspect(conn.body_params)}")
        %{conn.body_params | user: user}
      else
        conn = Plug.Conn.fetch_query_params(conn)
        params = HttpParameters.normalize(conn.query_params)

        Logger.info(
          "Serving query with parameters #{params.net}_#{params.sta}_#{params.loc}_#{params.cha} #{params.start} → #{params.end}"
        )

        # Transforms the requests parameters in a valid structure to be easily managed by the backend.
        %DataRequest{
          quality: params.quality,
          nodata: params.nodata,
          user: user,
          streams: [
            %Stream{
              net: params.net,
              sta: params.sta,
              loc: params.loc,
              cha: params.cha,
              start: params.start,
              end: params.end
            }
          ]
        }
      end

    datareq
    # Check if all stream are correctly specified
    |> DataRequest.fdsn_validate()
    # Expand streams having coma separated values.
    |> DataRequest.expand_streams()
    # Request inventory and generate deterministic streams
    |> DataRequest.match_streams_with_inventory()
    |> Backend.filter_permissions()
    |> Backend.evaluate_samples()
    |> Backend.get_data_files()
    |> Backend.prepare_workdir()
    |> make_response(conn)
  end

  @spec make_response(
          {:error | :toomuch | :eintern, String.t()}
          | {:nodata, DataRequest.t()}
          | DataRequest.t(),
          Plug.Conn.t()
        ) :: Plug.Conn.t()
  def make_response({:error, reason}, conn) do
    Logger.notice("Error handling the query. #{reason}")

    conn
    |> Plug.Conn.send_resp(400, "Some parameters are not valid: #{reason}")
  end

  def make_response({:eintern, reason}, conn) do
    Logger.error("Error handling the query. #{reason}")

    conn
    |> Plug.Conn.send_resp(
      500,
      "Internal server error. Please contact support, providing request id #{Logger.metadata()[:request_id]}"
    )
  end

  def make_response({:toomuch, reason}, conn) do
    Logger.notice("Too much data: #{reason}")

    conn
    |> Plug.Conn.send_resp(413, "Too much data: #{reason}")
  end

  def make_response({:nodata, datareq}, conn) do
    Logger.info("Nothing available in the archive")
    conn = Plug.Conn.send_resp(conn, datareq.nodata, "No data available for selection criteria.")
    # We need to get reqid in a local variable to pass it in a Task
    reqid = Backend.short_request_id()
    Task.start(fn -> Backend.send_statistics(conn, datareq, reqid) end)
    conn
  end


  def make_response(%DataRequest{} = datareq, conn) do
    # Read data from archive
    case Backend.read_all_data(datareq.files) do
      {:ok, data_dir} ->
        {time_in_microseconds, conn} =
          :timer.tc(fn ->
            send_data(conn, datareq, data_dir)
          end)

        # We need to get reqid in a local variable to pass it in a Task
        reqid = Logger.metadata()[:request_id]
        Task.start(fn -> Backend.send_statistics(conn, datareq, reqid) end)
        Logger.info("Data sent to client in #{time_in_microseconds / 1_000} ms")
        conn

      {:error, msg} ->
        make_response({:eintern, msg}, conn)
    end
  end

  @spec send_data(Plug.Conn.t(), DataRequest.t(), String.t()) :: Plug.Conn.t()
  def send_data(conn, datareq, datadir) do
    if length(File.ls!(datadir)) == 0 do
      make_response({:nodata, datareq}, conn)
    else
      {:ok, datetime} = DateTime.now("Etc/UTC")

      conn =
        conn
        |> Plug.Conn.put_resp_header(
          "content-disposition",
          "attachment; filename=fdsnws-make_response_#{DateTime.to_iso8601(datetime, :basic)}.mseed"
        )
        |> Plug.Conn.put_resp_header("content-type", "application/vnd.fdsn.mseed")
        |> Plug.Conn.send_chunked(200)

      # Send the data in chunks to the client
      Logger.debug("Reading data in #{datadir}")

      File.ls!(datadir)
      |> Enum.sort()
      |> Enum.map(&File.read!("#{datadir}/#{&1}"))
      |> Enum.reduce_while(conn, fn chunk, conn ->
        case Plug.Conn.chunk(conn, chunk) do
          {:ok, conn} ->
            {:cont, conn}

          {:error, reason} ->
            Logger.warning("Sending chunk failed: #{reason}")
            {:halt, conn}
        end
      end)
    end
  end

  @doc """
  Check the health of all the application and dependencies.
  - inventory database
  - authentication database
  - redis server
  - archive directory
  - bud directory

  """
  @spec health() :: {:ok, String.t()} | {:error, String.t()}
  def health do
    case Redix.command(:redix, ["PING"]) do
      {:ok, _} ->
        {:ok, "✓ Redis server #{Application.get_env(:wsdataselect, :redis_host)} ping"}
        |> Inventory.Repo.health()
        |> Auth.Repo.health()
        |> Archive.health(:valid_data_dir)
        |> Archive.health(:bud_data_dir)
        |> check_dataselect()

      {:error, _} ->
        {:ok,
         "💀 Redis server #{Application.get_env(:wsdataselect, :redis_host)} not accessible. Stats will not work"}
        |> Inventory.Repo.health()
        |> Auth.Repo.health()
        |> Archive.health(:valid_data_dir)
        |> Archive.health(:bud_data_dir)
        |> check_dataselect()
    end
  end

  defp check_dataselect({:error, m}) do
    {:error, m}
  end

  defp check_dataselect({:ok, m}) do
    ds = Application.get_env(:wsdataselect, :dataselect)

    if File.exists?(ds) do
      {:ok, m <> "\n✓ dataselect is present"}
    else
      {:error, m <> "\n💀 #{ds} not found"}
    end
  end

  def send_doc(conn) do
    priv_dir = :code.priv_dir(:wsdataselect)
    index_path = Path.join([priv_dir, "static", "index.html"])

    conn
    |> Plug.Conn.put_resp_header("content-type", "text/html; charset=utf-8")
    |> Plug.Conn.send_file(200, index_path)
  end
end
