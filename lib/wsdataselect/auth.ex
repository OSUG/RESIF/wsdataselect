defmodule Wsdataselect.Auth.Header do
  @moduledoc """
  Defines the structure for an authentication header.
  """
  @realm Application.compile_env(:wsdataselect, :realm)

  defstruct username: "lagaffe",
            response: "",
            nonce: "",
            opaque: "",
            uri: "",
            credential: "",
            realm: @realm

  @type t() :: %Wsdataselect.Auth.Header{
          username: String.t(),
          response: String.t(),
          nonce: String.t(),
          opaque: String.t(),
          uri: String.t(),
          credential: String.t(),
          realm: String.t()
        }
end

defmodule Wsdataselect.Auth do
  @moduledoc """
  Functions to authenticate and validate authorizations.
  """
  alias Wsdataselect.Auth.{Repo, WsUsers, Header}
  require Logger
  require Plug.Conn

  @realm Application.compile_env(:wsdataselect, :realm)

  @doc """
  Run authentication mechanism with HTTP Digest
  If there is no authorization header in the request, reply with a random opaque and nonce
  In case of successful authentication,
  Returns:
  {:challenge_set, header_value} if the request had no authorization header.
  {:ok, user} if the authentication was successful
  {:error, message} if the authentication failed or something went wrong in the authentication process
  """
  @spec authenticate(Plug.Conn.t()) :: {:ok | :error | :challenge_set, String.t()}
  def authenticate(conn) do
    case Plug.Conn.get_req_header(conn, "authorization") do
      [] ->
        # No authorization header, reply with a challenge
        Logger.debug("Got request without authentication header. Reply with a challenge.")
        # Generate opaque and nonce
        opaque = random_string(35)
        nonce = random_string(35)
        # Store them in the external store
        case Redix.command(:redix, ["SET", opaque, nonce]) do
          {:ok, "OK"} ->
            {:challenge_set,
             "Digest realm=\"#{@realm}\", nonce=\"#{nonce}\", opaque=\"#{opaque}\""}

          {:error, m} ->
            {:error, m}
        end

      [headers] ->
        # We got a challenge reply, let's validate it
        Logger.debug("Headers: #{inspect(headers)}")

        headers
        # Format all heades in a %Header{} structure
        |> headers_to_map()
        # Verify that the opaque:nonce corresponds to a previous challenge
        |> check_opaque()
        # Get the credential from the Auth database
        |> get_credential()
        |> validate_response()
        # Compute the challenge and check if the response is valid
        |> check_response(conn.method)
        # Delete the opaque:noce from the store
        |> del_opaque()
    end
  end

  @alphabet Enum.concat([?0..?9, ?a..?f])
  # Create a random string of count caracters
  @spec random_string(integer) :: String.t()
  defp random_string(count) do
    Stream.repeatedly(&random_char_from_alphabet/0)
    |> Enum.take(count)
    |> List.to_string()
  end

  defp random_char_from_alphabet do
    Enum.random(@alphabet)
  end

  # Transform headers text into a map
  # Example :
  # iex(0)> "Digest username=\"resifdb\", realm=\"FDSN\", nonce=\"68bd4018eb01d4e7698ff3d4bb6ea2491ae\", uri=\"/queryauth?start=2022-12-31&end=2023-01-02T01:00:00&net=FR&loc=01&cha=*&nodata=404&sta=CIEL\", response=\"41c0732abf99c297d5eb9973ae193f6b\", opaque=\"ac3b8db533d8734f83f4fb5f11f1e96e0a2\""
  # |> headers_to_map()
  #
  # %{
  #   username: "resifdb",
  #   nonce: "68bd4018eb01d4e7698ff3d4bb6ea2491ae",
  #   opaque: "ac3b8db533d8734f83f4fb5f11f1e96e0a2",
  #   realm: "FDSN",
  #   response: "41c0732abf99c297d5eb9973ae193f6b",
  #   uri: "/queryauth?start2022-12-31&end2023-01-02T01:00:00&netFR&loc01&cha*&nodata404&staCIEL"
  # }
  @spec headers_to_map(String.t()) :: Header.t()
  def headers_to_map(header) do
    header
    |> String.replace("Digest username", "username")
    |> String.split(", ")
    |> Enum.map(&String.split(&1, "="))
    |> Enum.reduce(%{}, fn [a | b], acc ->
      Map.put(acc, String.to_atom(a), Enum.join(b, "=") |> String.replace("\"", ""))
    end)
  end

  @spec check_opaque(Header.t()) :: {:ok, Header.t()} | {:error, String.t()}
  defp check_opaque(header) do
    case Redix.command(:redix, ["GET", header.opaque]) do
      {:ok, nonce} when nonce == header.nonce ->
        {:ok, header}

      {:ok, nil} ->
        {:error, "Unknown opaque"}

      {:ok, _} ->
        {:error, "nonce mismatch"}

      {:error, m} ->
        Logger.error(m)
        {:error, m}
    end
  end

  # Get credential from the database
  @spec get_credential({:error, String.t()} | {:ok, Header.t()}) ::
          {:error, String.t()} | {:ok, Header.t()}
  defp get_credential({:error, m}) do
    {:error, m}
  end

  defp get_credential({:ok, header}) do
    case Repo.get_by(WsUsers, login: header.username) do
      nil -> {:error, "Unknown user"}
      user -> {:ok, Map.put(header, :credential, user.digest)}
    end
  end

  @spec validate_response({:error, String.t()} | {:ok, Header.t()}) ::
          {:error, String.t()} | {:ok, Header.t()}
  defp validate_response({:error, m}) do
    {:error, m}
  end

  defp validate_response({:ok, header}) do
    case [:uri, :response, :nonce, :username, :realm, :opaque]
         |> Enum.all?(&Map.has_key?(header, &1)) do
      true -> {:ok, header}
      false -> {:error, "Missing mandatory keys in header"}
    end
  end

  @spec check_response({:error, String.t()} | {:ok, Header.t()}, String.t()) ::
          {:error, String.t()} | {:ok, Header.t()}
  defp check_response({:error, m}, _) do
    {:error, m}
  end

  defp check_response({:ok, header}, method) do
    # See https://datatracker.ietf.org/doc/html/rfc2617#section-3.1.3
    # HA1 = MD5(username:realm:password) -> can be retrieved as is from resifAuth.ws_users.user_pass
    # HA2 = MD5(method:digestURI)
    # response = MD5(HA1:nonce:HA2)
    a2 = :crypto.hash(:md5, "#{method}:#{header.uri}") |> Base.encode16() |> String.downcase()
    Logger.debug("Hashing: #{method}:#{header.uri} to #{a2}")

    computed_response =
      :crypto.hash(:md5, "#{header.credential}:#{header.nonce}:#{a2}")
      |> Base.encode16()
      |> String.downcase()

    Logger.debug("Hashing: #{header.credential}:#{header.nonce}:#{a2}")
    Logger.debug("Computed response: #{computed_response}")
    Logger.debug("Client response:   #{header.response}")

    if computed_response == header.response do
      {:ok, header}
    else
      {:error, "Authentication failed for user #{header.username}"}
    end
  end

  @spec del_opaque({:error, String.t()} | {:ok, Header.t()}) :: {:error | :ok, String.t()}
  defp del_opaque({:error, m}) do
    {:error, m}
  end

  defp del_opaque({:ok, header}) do
    case Redix.command(:redix, ["DEL", header.opaque]) do
      {:ok, _} -> {:ok, header.username}
      {:error, reason} -> {:error, reason}
    end
  end
end
