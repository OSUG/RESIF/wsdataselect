defmodule Wsdataselect.Parallel do
  def pmap(collection, func) do
    collection
    |> Enum.map(&Task.async(fn -> func.(&1) end))
    |> Enum.map(&Task.await(&1, 30000))
  end

  @spec pmap_chunk(Enumerable.t(), integer(), function()) :: list
  def pmap_chunk(collection, chunk, func) do
    collection
    |> Enum.chunk_every(chunk)
    |> Enum.flat_map(fn subc -> pmap(subc, func) end)
  end
end
