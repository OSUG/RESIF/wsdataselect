defmodule Wsdataselect.Stream do
  @moduledoc """
  A stream is a combination of parameters: Network, Station, Location, Channel, start and end.
  Samplerate is useful in order to evaluate the number of samples on the stream.

  There is also some helper functions in order to manipulate streams.
  """
  alias Wsdataselect.Inventory.{Repo, Network, Station, Channel}
  import Ecto.Query
  require Logger

  defstruct net_id: 0,
            net: "*",
            sta: "*",
            loc: "*",
            cha: "*",
            samplerate: 0,
            start: DateTime.utc_now(),
            end: DateTime.utc_now()

  @typedoc """
  A stream NSLC, start and end. Samplerate is useful for samples evaluation.
  """
  @type t() :: %Wsdataselect.Stream{
          net_id: integer,
          net: String.t(),
          sta: String.t(),
          loc: String.t(),
          cha: String.t(),
          samplerate: integer,
          start: DateTime.t() | String.t(),
          end: DateTime.t() | String.t()
        }

  @doc """
  When the network is defined as a wildcard, split the stream in multiple streams.
  Returns a list of stream (possibly empty)
  # Parameters

  - stream : a %Wsdaselect.Stream struct

  # Examples

  iex(0)> Wsdataselect.Stream.expand_on_network(%{net: "F%", sta: "CIEL", loc: "01", cha: "H__", start: DateTime.utc_now(), end: DateTime.utc_now()})

  [
    %{
      cha: "H__",
      end: ~U[2023-08-24 07:28:25.218111Z],
      loc: "01",
      net: "FR",
      net_id: 39,
      samplerate: 0,
      sta: "CIEL",
      start: ~U[2023-08-24 07:28:25.218102Z]
    },
    %{
      cha: "H__",
      end: ~U[2023-08-24 07:28:25.218111Z],
      loc: "01",
      net: "FO",
      net_id: 68,
      samplerate: 0,
      sta: "CIEL",
      start: ~U[2023-08-24 07:28:25.218102Z]
    }
  ]
  """
  @spec expand_on_network(t()) :: list(t())
  def expand_on_network(stream) do
    Logger.debug("Expanding on network #{inspect(stream)}")

    net_list =
      case Cachex.fetch(
             :network,
             "#{stream.net}::#{stream.sta}",
             &expand_on_network_db_fallback/1
           ) do
        {:ok, result} ->
          result

        {:commit, result} ->
          result

        {:error, m} ->
          Logger.error("Problem while getting #{stream.net}::#{stream.sta} from cache: #{m}")
          []
      end

    Enum.map(net_list, fn l -> Map.merge(stream, l) end)
  end

  defp expand_on_network_db_fallback(key) do
    [net, sta] = String.split(key, "::")

    query =
      from(n in Network,
        join: s in Station,
        on: n.network_id == s.network_id,
        where: like(s.station, ^sta),
        where: like(n.network, ^net),
        select: %{net: n.network, net_id: n.network_id},
        distinct: true
      )

    Logger.debug("Expanding on networks: #{inspect(query)}")

    case Repo.all(query) do
      nil -> []
      list when is_list(list) -> list
    end
  end

  @doc """
  This function matches the parameters wanted in the request with the actual metadata defined in the inventory.
  Using one request to the inventory, this function expands a stream to:
  - expand all wildcards in possibily separated streams
  - adjust start and end time of the request to the defined start and entime in the metadata (intersect)
  This can result in splitting the stream in several parts (even if real world use case is rare).
  By definition a stream is always a coninuous timespan.

  ```
  Stream:   [-----------]
  Metadata:    [+++]  [++++]
  Intersect:   [***]  [*]
  ```
  """
  @spec match_with_inventory(t() | {:error, String.t()}) :: list(t()) | {:error, String.t()}
  def match_with_inventory({:error, msg}) do
    {:error, msg}
  end

  def match_with_inventory(stream) do
    # Get all inventory matching the stream with wildcards
    query =
      from(c in Channel,
        join: s in assoc(c, :station),
        as: :station,
        join: n in assoc(s, :network),
        as: :network,
        select: %Wsdataselect.Stream{
          net: n.network,
          sta: s.station,
          loc: c.location,
          cha: c.channel,
          start: c.starttime,
          end: c.endtime
        },
        where: like(n.network, ^translate_wildcards(stream.net)),
        where: like(s.station, ^translate_wildcards(stream.sta)),
        where: like(c.location, ^translate_wildcards(stream.loc)),
        where: like(c.channel, ^translate_wildcards(stream.cha)),
        where: c.starttime <= ^stream.end,
        where: c.endtime >= ^stream.start
      )

    epochs = Repo.all(query)
    Logger.debug("Fetched #{length(epochs)} epochs in database: #{inspect(epochs)}")
    # Return a list of streams intersected with each epochs.
    epochs
    |> Enum.map(fn e -> intersection(e, stream) end)
    |> Enum.filter(fn e -> e != :empty end)
  end

  # Transforms the wildcards caracter from the webservice specification into SQL wildcards
  # Returns a string with SQL wildcards
  @spec translate_wildcards(String.t()) :: String.t()
  defp translate_wildcards(s) do
    s |> String.replace("*", "%") |> String.replace("?", "_")
  end

  @doc """
  Check if a stream is valid or invalid

  Returns true if the stream is not valid
  """
  @spec valid?(t()) :: boolean()
  def valid?(stream) do
    Logger.debug("Validating stream #{inspect(stream)}")

    case validate(stream) do
      {:ok, _} ->
        true

      {:error, e} ->
        Logger.error("Stream #{inspect(stream)} is not valid: #{inspect(e)}")
        false
    end
  end

  @doc """
  Check if a stream is valid.

  Returns {:ok, stream} if valid, {:error, reason} if invalid.
  The stream returned has it's start and end parsed.

  ## Examples

  iex(4)> Wsdataselect.Stream.validate(%Wsdataselect.Stream{net: "FR", sta: "CIEL", loc: "00", cha: "HN?", start: "2023-08-25T07:30:26", end: "2023-08-25T07:30:26" })
  {:ok,
  %Wsdataselect.Stream{
    net: "FR",
    sta: "CIEL",
    loc: "00",
    cha: "HN?",
    samplerate: 0,
    start: ~U[2023-08-25 07:30:26Z],
    end: ~U[2023-08-25 07:30:26Z]
  }}
  """
  @spec validate(t()) :: {:ok, t()} | {:error, String.t()}
  def validate(stream) do
    {:ok, stream}
    |> validate_net()
    |> validate_sta()
    |> validate_loc()
    |> validate_cha()
    |> validate_date(:start)
    |> validate_date(:end)
    |> validate_order()
  end

  @spec validate_net({:ok, t()} | {:error, String.t()}) :: {:ok, t()} | {:error, String.t()}
  defp validate_net({:ok, stream}) do
    cond do
      # The regex also match repeted coma separated values
      Regex.match?(~r/^[A-Z0-9\?\*]{1,2}(?:,[A-Z0-9\?\*]{1,2})*$/, stream.net) -> {:ok, stream}
      stream.net == "*" -> {:ok, stream}
      true -> {:error, "Invalid network name in stream: #{inspect(stream)}"}
    end
  end

  @spec validate_sta({:ok, t()} | {:error, String.t()}) :: {:ok, t()} | {:error, String.t()}
  defp validate_sta({:error, stream}) do
    {:error, stream}
  end

  defp validate_sta({:ok, stream}) do
    cond do
      Regex.match?(~r/^[A-Z0-9\?\*]{2,5}(?:,[A-Z0-9\?\*]{2,5})*$/, stream.sta) -> {:ok, stream}
      stream.sta == "*" -> {:ok, stream}
      true -> {:error, "Invalid station name in stream: #{inspect(stream)}"}
    end
  end

  @spec validate_loc({:ok, t()} | {:error, String.t()}) :: {:ok, t()} | {:error, String.t()}
  defp validate_loc({:error, stream}) do
    {:error, stream}
  end

  defp validate_loc({:ok, stream}) do
    cond do
      Regex.match?(~r/^[A-Z0-9\?\*-]{2}(?:,[A-Z0-9\?\*-]{2})*$/, stream.loc) -> {:ok, stream}
      stream.loc == "*" -> {:ok, stream}
      true -> {:error, "Invalid location in stream: #{inspect(stream)}"}
    end
  end

  @spec validate_cha({:ok, t()} | {:error, String.t()}) :: {:ok, t()} | {:error, String.t()}
  defp validate_cha({:error, stream}) do
    {:error, stream}
  end

  defp validate_cha({:ok, stream}) do
    # Celui là est plus compliqué. Mieux vaut splitter sur la ,
    if String.split(stream.cha, ",") |> Enum.all?(&valid_cha?/1) do
      {:ok, stream}
    else
      {:error, "Invalid channel in stream: #{inspect(stream)}"}
    end
  end

  defp valid_cha?(cha) do
    cond do
      String.length(cha) < 3 and String.contains?(cha, "*") -> true
      Regex.match?(~r/[\?\*A-Z0-9][\*\?A-Z0-0][\*\?A-Z0-9]/, cha) -> true
      cha == "*" -> true
      true -> false
    end
  end

  # Check if the date is valid. If it's OK, returns a Stream with the date modified as a DateTime struct.
  @spec validate_date({:ok, t()} | {:error, String.t()}, atom) ::
          {:ok, t()} | {:error, String.t()}
  defp validate_date({:error, reason}, _) do
    {:error, reason}
  end

  defp validate_date({:ok, stream}, key) do
    Logger.debug("Validating #{key} for #{inspect(stream)}")

    case parse_date(Map.fetch!(stream, key)) do
      {:ok, d, _} -> {:ok, Map.replace(stream, key, d)}
      _ -> {:error, "Invalid date #{key}: #{Map.fetch!(stream, key)}"}
    end
  end

  @doc """
  Parse the dates of a Stream and returns a stream with %DateTime{} at start and end
  If some date parsing went wrong, returns {:error, reason}

  # Parameters

  - stream : an %Wsdataselect.Stream{}
  """
  @spec parse_dates(t()) :: t() | {:error, String.t()}
  def parse_dates(stream) do
    with {:ok, starttime, 0} <- parse_date(stream.start),
         {:ok, endtime, 0} <- parse_date(stream.end) do
      %{stream | start: starttime, end: endtime}
    else
      {:error, _} -> {:error, "Error parsing date in #{inspect(stream)}"}
    end
  end

  # Parse a date from all accepted formats in the FDSN norm.
  # - just a date
  # - a date with time separated by "T"
  # All dates are assumed to be UTC
  # Return a tupple {:ok, DateTime} or {:error, "error message"}

  # ## Examples

  # iex> Wsdataselect.Validator.parse_date("2023-01-01")
  # iex> {:ok, ~U[2023-01-01 00:00:00.000Z], 0}

  # iex> Wsdataselect.Validator.parse_date("2023-01-01T23:59:59.999")
  # iex> {:ok, ~U[2023-01-01 23:59:59.999Z], 0}

  # iex> Wsdataselect.Validator.parse_date("2023-02-31T23:59:59.999")
  # iex> {:error, :invalid_format}

  # iex> Wsdataselect.Validator.parse_date("currentutcday")
  # iex> {:ok, _, 0}
  @spec parse_date(DateTime.t() | String.t()) :: {:ok, DateTime.t(), integer} | {:error, atom()}
  defp parse_date(%DateTime{} = d) do
    {:ok, d, 0}
  end

  defp parse_date(date_string) do
    re_date = ~r/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/
    re_datetime = ~r/^[0-9]{4}-[0-9]{2}-[0-9]{2}T/

    cond do
      Regex.match?(re_date, date_string) ->
        DateTime.from_iso8601(date_string <> "T00:00:00.000Z")

      Regex.match?(re_datetime, date_string) ->
        DateTime.from_iso8601(date_string <> "Z")

      "currentutcday" == date_string ->
        Logger.debug("Set time at midnight")
        Tuple.append(Date.utc_today() |> DateTime.new(~T[00:00:00]), 0)

      true ->
        DateTime.from_iso8601(date_string)
    end
  end

  @spec validate_order({:ok, t()} | {:error, String.t()}) :: {:ok, t()} | {:error, String.t()}
  defp validate_order({:error, reason}) do
    {:error, reason}
  end

  defp validate_order({:ok, stream}) do
    if DateTime.compare(stream.end, stream.start) == :lt do
      {:error, "Endtime before starttime"}
    else
      Logger.debug("Parameters are fine: #{inspect(stream)}")
      {:ok, stream}
    end
  end

  @doc """
  If the stream has multiple values on :net, :sta, :loc or :cha, then the function splits the stream and returns a list of stream.
  If there is nothing to expand, it returns a list with only the stream given in parameter.

  ## Parameters
  - stream: a %Wsdataselect.Stream{} structure

  ## Examples

  iex> Wsdataselect.Stream.expand_multi_values(%{net: "FR,RA", sta: "CIEL,ABCD", loc: "00,01", cha: "HNZ,HNE"})
  [
    %{cha: "HNZ", loc: "00", net: "FR", sta: "CIEL"},
    %{cha: "HNZ", loc: "00", net: "RA", sta: "CIEL"},
    %{cha: "HNZ", loc: "00", net: "FR", sta: "ABCD"},
    %{cha: "HNZ", loc: "00", net: "RA", sta: "ABCD"},
    %{cha: "HNZ", loc: "01", net: "FR", sta: "CIEL"},
    %{cha: "HNZ", loc: "01", net: "RA", sta: "CIEL"},
    %{cha: "HNZ", loc: "01", net: "FR", sta: "ABCD"},
    %{cha: "HNZ", loc: "01", net: "RA", sta: "ABCD"},
    %{cha: "HNE", loc: "00", net: "FR", sta: "CIEL"},
    %{cha: "HNE", loc: "00", net: "RA", sta: "CIEL"},
    %{cha: "HNE", loc: "00", net: "FR", sta: "ABCD"},
    %{cha: "HNE", loc: "00", net: "RA", sta: "ABCD"},
    %{cha: "HNE", loc: "01", net: "FR", sta: "CIEL"},
    %{cha: "HNE", loc: "01", net: "RA", sta: "CIEL"},
    %{cha: "HNE", loc: "01", net: "FR", sta: "ABCD"},
    %{cha: "HNE", loc: "01", net: "RA", sta: "ABCD"}
  ]
  """
  @spec expand_multi_values(t()) :: list(t())
  def expand_multi_values(stream) do
    [stream]
    |> Enum.flat_map(&expand_multi_values(&1, :cha))
    |> Enum.flat_map(&expand_multi_values(&1, :loc))
    |> Enum.flat_map(&expand_multi_values(&1, :sta))
    |> Enum.flat_map(&expand_multi_values(&1, :net))
  end

  defp expand_multi_values(stream, key) do
    Logger.debug("Stream to expand: #{inspect(stream)} on #{key}")

    case Map.fetch(stream, key) do
      :error ->
        :error

      {:ok, val} ->
        val
        |> String.split(",")
        |> Enum.map(fn v -> Map.replace(stream, key, v) end)
    end
  end

  @doc """
  Instersect the first stream with the second one.
  Returns the first stream with dates intersected with the second stream.
  If no intersection foud, returns :empty
  """
  @spec intersection(t(), t()) :: t() | :empty
  def intersection(%Wsdataselect.Stream{} = s1, %Wsdataselect.Stream{} = s2) do
    Logger.debug("Getting intersection between #{inspect(s1)} and #{inspect(s2)}")

    if match_nslc?(s1, s2) do
      Logger.debug("Computing intersection between #{inspect(s1)} and #{inspect(s2)}")

      newstart =
        case DateTime.before?(s1.start, s2.start) do
          true -> s2.start
          false -> s1.start
        end

      newend =
        case DateTime.before?(s1.end, s2.end) do
          true -> s1.end
          false -> s2.end
        end

      Logger.debug("Intersect result: #{newstart} → #{newend}")

      case DateTime.before?(newend, newstart) do
        true ->
          Logger.info(
            "No intersection between epoch #{inspect(s1)} and requests stream #{inspect(s2)}"
          )

          :empty

        false ->
          %{s1 | start: newstart, end: newend}
      end
    else
      Logger.debug("Not the same NSLC in #{inspect(s1)} and #{inspect(s2)}, pass")
      :empty
    end
  end

  @doc """
  Test if NSLC match between 2 streams.
  The second stream can have wildcards.
  """
  @spec match_nslc?(t(), t()) :: boolean()
  def match_nslc?(s1, s2) do
    Regex.match?(fdsn_regex(s2.net), s1.net) and Regex.match?(fdsn_regex(s2.sta), s1.sta) and
      Regex.match?(fdsn_regex(s2.loc), s1.loc) and Regex.match?(fdsn_regex(s2.cha), s1.cha)
  end

  # From FDSN wildcard spec to a normal regex
  @spec fdsn_regex(String.t()) :: Regex.t()
  defp fdsn_regex(s) do
    regex_str = String.replace(s, "*", ".*") |> String.replace("?", ".?")
    Regex.compile!("^#{regex_str}$")
  end
end
