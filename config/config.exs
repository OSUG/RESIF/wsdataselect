import Config

config :wsdataselect, Wsdataselect.Inventory.Repo, types: Wsdataselect.PostgrexTypes
config :wsdataselect, Wsdataselect.Auth.Repo, pool_size: 3

# Realm is used fot HTTP Digest authentication.
# As the value is hardcoded in the authentication database's credentials,
# Let's fix it at compilation time.
config :wsdataselect, realm: System.get_env("AUTH_REALM", "FDSN")

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id, :application]

config :sentry,
  enable_source_code_context: true,
  root_source_code_paths: [File.cwd!()]

import_config "#{Mix.env()}.exs"
