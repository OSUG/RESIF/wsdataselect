import Config

dbuser = System.get_env("DBUSER", "wsdataselect")
dbpass = System.get_env("DBPASS", "wsdataselect")
dbhost = System.get_env("DBHOST", "localhost")
dbport = System.get_env("DBPORT", "5432")
invdb = System.get_env("INVDB", "resifInv")
invdb_pool_size = "INVDB_POOL_SIZE" |> System.get_env("10") |> String.to_integer()
authdb = System.get_env("AUTHDB", "resifAuth")
maxsamples = "MAXSAMPLES" |> System.get_env("2000000000") |> String.to_integer()
dataselect_timeout = "DATASELECT_TIMEOUT" |> System.get_env("20000") |> String.to_integer()
dataselect_concurrency = "DATASELECT_CONCURRENCY" |> System.get_env("8") |> String.to_integer()
dataselect_max_files = "DATASELECT_MAX_FILES_PER_PROCESS"|> System.get_env("12") |> String.to_integer()
post_max_lines = "POST_MAX_LINES" |> System.get_env("100") |> String.to_integer()
valid_data_dir = System.get_env("VALID_DATA_DIR", "/data")
bud_data_dir = System.get_env("BUD_DATA_DIR", "/bud")
redis_host = System.get_env("REDIS_HOST", "localhost")
redis_stats_queue = System.get_env("REDIS_STATS_QUEUE", "fdsnwsstat")
workdir = System.get_env("WORK_DIR", "/tmp")

pg_prepare_statement = case System.get_env("PG_PREPARE_STATEMENT", "named") do
  "unnamed" -> :unnamed
  _ -> :named
end

config :wsdataselect, dbuser: dbuser
config :wsdataselect, dbpass: dbpass
config :wsdataselect, dbhost: dbhost
config :wsdataselect, dbport: dbport
config :wsdataselect, invdb: invdb
config :wsdataselect, invdb_pool_size: invdb_pool_size
config :wsdataselect, authdb: authdb
config :wsdataselect, maxsamples: maxsamples
config :wsdataselect, valid_data_dir: valid_data_dir
config :wsdataselect, bud_data_dir: bud_data_dir
config :wsdataselect, redis_host: redis_host
config :wsdataselect, redis_stats_queue: redis_stats_queue
config :wsdataselect, dataselect: System.find_executable("dataselect")
config :wsdataselect, workdir: workdir
config :wsdataselect, dataselect_timeout: dataselect_timeout
config :wsdataselect, post_max_lines: post_max_lines
config :wsdataselect, dataselect_concurrency: dataselect_concurrency
config :wsdataselect, dataselect_max_files_per_process: dataselect_max_files
config :wsdataselect, prepare_statement: pg_prepare_statement

config :wsdataselect, Wsdataselect.Inventory.Repo,
  prepare: pg_prepare_statement
config :wsdataselect, Wsdataselect.Inventory.Repo,
  pool_size: invdb_pool_size
config :wsdataselect, Wsdataselect.Auth.Repo,
  prepare: pg_prepare_statement
