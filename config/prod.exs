# Configuration at compile time for production release
#
import Config

IO.puts("Adding prod configuration")
config :logger,
  compile_time_purge_matching: [
    [level_lower_than: :info],
    [application: :ecto_sql]
  ]
