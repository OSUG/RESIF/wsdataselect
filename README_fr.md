# Description du programme Wsdataselect

Wsdataselect est une implémentation du web service dataselect tel que spécifié par la FDSN (https://www.fdsn.org/webservices/).

Il contient des spécificités propres au centre de données sismmologique EPOS-France:
- schéma de base de donnée d'inventaire (`Wsdataselect.Inventory`)
- schéma pour l'authentification (`Wsdataselect.Auth`)
- système de statistiques (`Wsdataselect.Statistics`)

## Choix technologiques

Le programme est écrit en Elixir, avec les choix d'implémentation suivant:
- serveur HTTP Bandit https://github.com/mtrudel/bandit
- interaction avec la base postgresql via Ecto https://github.com/elixir-ecto/ecto
- gestion des requêtes HTTP avec Plug https://hexdocs.pm/plug
- gestion des exceptions et profiling avec Sentry https://www.sentry.io/

## Fonctionnement

Les requêtes sont traitées avec la librairie Plug par `Wsdataselect.Router`. C'est le point d'entrée.

Le routeur appelle les logiques de traitement implémentées dans le `Wsdataselect.Controller`.

Le `Wsdataselect.Controller` fait:
- l'authentification si nécessaire
- séquençage (parsing) des paramètres de la requête
- l'analyse de la requête et sa validité à l'aide du module `Wsdataselect.DataRequest`

Puis, `Wsdataselect.Backend` prend le relais et réalise:
- l'identification de tous les fichiers de données à ouvrir
- la lecture des données de l'archive 
- la transmission de la réponse au client 
- l'envoi des informations à des fins statistiques `Wsdataselect.Statistics`

L'interaction avec l'inventaire et l'archive de données via le module `Wsdataselect.Backend`
  - vérification des permissions `Wsdataselect.Auth`
  - interrogation de l'inventaire `Wsdataselect.Inventory`

Chaque module contient une documentation donnant tous les détails de son fonctionnement.
  
### Routeur
Le routeur est basé sur Plug. Il est assez basique et suit les étapes classiques documentées par la librairie (https://github.com/elixir-plug/plug).

Il gère les //endpoints// suivant
- GET /query
- GET /queryauth
- POST /query
- POST /queryauth
- GET /health
- Quelques fichiers classiques comme la documentation et la description du webservice au format WADL (`/application.wadl`)

### Authentification

La spécification fdsnws-dataselect-1 décrit l'authentification en HTTP Digest (https://datatracker.ietf.org/doc/html/rfc2617).

Le module `Wsdataselect.Auth` implémente ce protocole. Il est appelé par `Wsdataselect.Controller.authenticate/1`.

À la suite de cette étape, l'utilisateur est indiqué dans la structure `Wsdataselect.DataRequest` soit comme "anonymous" soit avec son nom d'utilisateur. On considère dès lors que l'utilisateur a été correctement authentifié.

### Séquençage des paramètres 
Pour une requête GET, la fonction `Wsdataselect.HttpParameters.normalize/1` permet de prendre les paramètres apparaissant sur l'URI de la requête pour en créer une structure `%Wsdataselect.DataRequest{}`.

Pour une requête POST, on a implémenté un module `Wsdataselect.BodyParser` qui prend le @behavior d'un  `Plug.Parsers`. La fonction `Wsdataselect.BodyParser.parse/5` est appelée automatiquement lors d'une requête POST. La structure `%Wsdataselect.DataRequest{}` qui en résulte est ajoutée à la structure `%Plug.Conn{}`.

### Analyse de la requête et validation
À cette étape, le contrôleur dispose des paramètres tels que soumis par l'utilisateur. Il faut maintenant les valider.

La fonction `Wsdataselect.DataRequest.fdsn_validate/1` effectue les validations suivantes:
- valide la valeur du paramètre nodata (204 ou 404)
- valide le code qualité indiqué
- valide chaque stream
- analyse les dates de début et de fin fournies par l'utilisateur

À ce stade, que ce soit une requête POST ou GET, le travail est le même. C'est la fonction `Wsdataselect.Controller.manage_query/2` qui déroule les étapes suivantes.

### Transposition des jokers

Les caractères jokers dans les streams sont traduits en leur équivalut SQL par `Wsdataselect.Stream.translate_wildcards/1`.

### Vérification des permissions

La fonction `Wsdataselect.Backend.filter_permissions/1` filtre les demandes de l'utilisateur afin de retirer les Streams pour lesquels il n'a pas de privilège d'accès.
La politique d'accès à la donnée est gérée au niveau du code réseau, donc, chaque Stream contenant un joker sur le code réseau est éclaté en plusieurs Streams (c'est la fonction `Wsdataselect.Stream.expand_on_network/1`).

### Évaluation du volume de données
Afin de ne pas servir les requêtes trop volumineuses, on évalue le volume de données avec la fonction `Wsdataselect.Backend.evaluate_samples/1`.

Celle ci interroge la base de donnée pour chaque stream et calcule le nombre de samples comme le produit de la durée en seconde et du samplerate.
Dès que la somme des samples dépasse la limite définie par la variable d'environnement `MAXSAMPLES`, alors on peut renvoyer à l'utilisateur un "Too much data".

Si l'évaluation vaut 0, alors c'est qu'il n'y a pas de donnée disponible, on renvoie tout de suite un "No data".

### Lecture des données
C'est la fonction `Wsdataselect.Backend.dataselect/2` qui réalise ce travail.

#### Récupération de la liste de fichiers
À ce stade, on récupère la liste des fichiers dans l'inventaire, avec la fonction `Wsdataselect.Backend.files_in_archive/1`.

De la liste récupérée dans l'inventaire, on retire tous les fichiers qui n'existent pas dans l'archive.

Si la liste de fichiers est vide, on renvoie un "no data". Des messages d'erreur sont écrits dans les logs de l'application pour chaque fichier non trouvé dans l'archive.

#### Lancement de dataselect
On peut maintenant lire les données et les transmettre à l'utilisateur. Pour cela on s'appuie sur le programme `dataselect` (https://github.com/EarthScope/dataselect/). 

Il faut lui fournir une liste de sélection et la liste des fichiers.

#### Transmission de la réponse au client
Les données transmises par dataselect sont directement transmises à l'utilisateur. Il n'y a pas d'écriture sur disque.
#### Envoi des informations pour les statistiques
Les données lues sont alors envoyées pour des statistiques.


## Déploiement
Pour déployer `wsdataselect`, il faut un certain nombre de prérequis, configurer les différentes options et lancer une instance.

### Prérequis
- Base de données d'inventaire: Celle ci doit être hébergée sur un serveur postgres.
- Base de donnée d'authentification: idem, il faut que les deux bases soient sur le même serveur postgres
- Serveur Redis: utilisé pour l'authentification et pour l'envoi de statistiques
- Archive de données sismologiques validées, organisées en fichiers journaliers, par code réseau: `NET/YEAR/STA/CHA.D/`
- Archive de données sismologiques brutes, organisée en fichiers journaliers par année `YEAR/NET/STA/CHA.D/`
- programme dataselect compilé et dans le PATH de l'application (voir https://github.com/EarthScope/dataselect/)

### Configuration

Voici la liste des configurations ainsi que les valeurs par défaut

``` 
| DBUSER               | wsdataselect | Utilisateur pour le SGBD                                                 |
| DBPASS               | wsdataselect | Mot de passe pour le SGBD                                                |
| DBHOST               | localhost    | Serveur postgres                                                         |
| DBPORT               | 5432         | Port d'accès au SGBD                                                     |
| INVDB                | resifInv     | Nom de la base de données d'inventaire                                   |
| AUTHDB               | resifAuth    | Nom de la base de données d'authentification                             |
| MAXSAMPLES           | 2000000000   | Nombre de samples maximum qu'une requête peut obtenir (2 milliards)      |
| DATASELECT_TIMEOUT   | 20000        | Temps maximal en millisecondes pour que dataselect fournisse les données |
| POST_MAX_LINES       | 100          | Nombre de ligne maximales pour une requête POST                          |
| VALID_DATA_DIR       | /data        | Chemin vers les données validées                                         |
| BUD_DATA_DIR         | /bud         | Chemin vers les données brutes                                           |
| REDIS_HOST           | localhost    | Serveur REDIS                                                            |
| REDIS_QUEUE          | fdsnwsstat   | Queue REDIS pour les statistiques                                        |
| WORKDIR              | workdir      | Répertoire de travail pour quelques fichiers temporaires                 |
| PG_PREPARE_STATEMENT | named        | Stratégie pour "prepare statement" postgres (named ou unnamed)           |     
```

#### Note à propos du HTTP Digest Realm

Ce paramètre vaut par défaut "FDSN", sa valeur fait partie du hash du credential permettant d'authentifier les utilisateurs. On peut la changer au moment de la compilation :

    REALM="MyRealm" mix compile
    
Mais pour le fonctionnement à Résif, il faut laisser la valeur par défaut.

#### Note à propos de pgbouncer
Pour que cette application fonctionne avec pgbouncer en mode "transaction pooling", il faut forcer les statements à passer en "unnamed". Cela peut se faire avec la variable d'environnement `PG_PREPARE_STATEMENT`.
### Container

On peut lancer l'outil à partir des containers publiés sur la forge Gricad Gitlab (https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/wsdataselect/container_registry/931)

### Compilation et lancement à partir des sources

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/wsdataselect.git
    cd wsdataselect
    mix deps.get
    mix run --nohalt

## À propos du backend dataselect

Le choix technique d'utiliser le programme dataselect pour lire la donnée dans l'archive a été avant tout dicté par une mise en œuvre plus simple. Cependant, le lancement d'un programme externe dans le but de créer un flux de données vers l'utilisateur via le protocole HTTP a été un peu compliqué à mettre en œuvre.

### Concernant les performances

L'archive est mise à disposition du programme via un montage NFS, ce qui induit une latence incompressible dans l'accès aux données. 

Sans prétendre à une caractérisation précise des performances, j'ai quand même réalisé quelques tests afin de voir ce qui est le plus pénalisant dans l'accès à la donnée.

#### Latence d'accès d'une donnée en début, milieu ou fin de fichier

À l'aide du binaire dataselect, on lance la lecture d'une heure de donnée d'un fichier en spécifiant une heure de la journée : 00:00:00, 12:00:00 et 23:00:00

Pour chaque lecture, on change le fichier afin de ne pas bénéficier d'un effet de cache


Par exemple pour la dernière heure d'un fichier:

    time dataselect -Pr -ts 2023,001,23,00,00 /mnt/nfs/summer/validated_seismic_data/FR/2023/CIEL/HNZ.D/FR.CIEL.01.HNZ.D.2023.001 -o plop.msi

    real	0m0.160s
    user	0m0.003s
    sys	0m0.009s
    
En répétant cette expérience pour plusieurs fichiers, les valeurs myennes sont comparables à l'exemple ci dessous.

Puis pour la 1ere heure d'un fichier similaire:

    time dataselect -Pr -ts 2023,006,00,00,00 -te 2023,006,01,00,00 /mnt/nfs/summer/validated_seismic_data/FR/2023/CIEL/HNZ.D/FR.CIEL.01.HNZ.D.2023.006 -o plop.msi

    real	0m0.149s
    user	0m0.003s
    sys	0m0.009s
    
Et pour l'heure du milieu d'un fichier:

    time dataselect -Pr -ts 2023,008,12,00,00 -te 2023,008,12,00,00 /mnt/nfs/summer/validated_seismic_data/FR/2023/CIEL/HNZ.D/FR.CIEL.01.HNZ.D.2023.008 -o plop.msi

    real	0m0.189s
    user	0m0.003s
    sys	0m0.007s
    
Donc la latence est comparable, quelle que soit le moment dans la journée que l'on considère.


#### Latence d'accès d'une donnée en précisant l'offset exact

Si on fait le même protocole en accédant à la donnée uniquement par des offsets, alors on obtien une latence bien plus faible:

    time dataselect  /mnt/nfs/summer/validated_seismic_data/FR/2023/CIEL/HNZ.D/FR.CIEL.01.HNZ.D.2023.028@11554816:12087296  -o plop.msi && msi -tg plop.msi
    
    real	0m0.063s
    user	0m0.000s
    sys	0m0.003s


Et si on combine les offsets de début et de fin avec des sélections de temps, on obtien aussi de meilleurs resultats:

    export doy=035 ; time dataselect  /mnt/nfs/summer/validated_seismic_data/FR/2023/CIEL/HNZ.D/FR.CIEL.01.HNZ.D.2023.${doy}@11554816 -ts 2023,${doy},22,50,00 -te 2023,${doy},23,50,00  -o plop.msi && msi -tg plop.msi

    real	0m0.053s
    user	0m0.000s
    sys	0m0.001s

On pourrait donc obtenir un gain significatif en exploitant les offsets de la donnée. Cela présuppose d'indexer la donnée, par exemple heure par heure.
    
