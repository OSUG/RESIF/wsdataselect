defmodule Wsdataselect.ParallelTest do
  require Logger
  use ExUnit.Case
  alias Wsdataselect.Parallel
  test "Parallel map +1" do
    res = Parallel.pmap 1..10, &(1 + &1)
    assert res == [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  end

  test "Parallel reduce" do
    res = Parallel.pmap(["1","2","3"], &(String.to_integer &1)) |> Enum.reduce(fn x, acc -> x+acc end)
    assert res == 6

  end

end
