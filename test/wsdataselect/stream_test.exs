defmodule Wsdataselect.StreamTest do
  require Logger
  alias Wsdataselect.Stream
  use ExUnit.Case
  doctest Wsdataselect.Stream

  test "Parse correct dates in stream" do
    parsed_stream = Stream.parse_dates(%Stream{start: "2020-01-01T00:00:00.1234556", end: "2020-01-02T00:23:23"})
    assert is_struct(parsed_stream, Stream)
  end
  test "Parse bad dates in stream" do
    parsed_stream = Stream.parse_dates(%Stream{start: "202-01-01T00:00:00.1234556", end: "2020-01-02T00:23:23"})
    assert {:error, _} = parsed_stream
  end

  test "Intersect simple streams" do
    s1 = %Stream{start: ~U[2020-01-01 00:00:00Z], end: ~U[2020-01-02 00:23:23Z]}
    s2 = %Stream{start: ~U[2020-01-01 12:00:00Z], end: ~U[2020-01-05 00:23:23Z]}
    si = Stream.intersection(s1, s2)
    assert si.start == s2.start
    assert si.end == s1.end
  end

  test "Intersect unmathed streams should return :empty" do
    s1 = %Stream{net: "FR", sta: "CIEL", loc: "00", cha: "HNZ", start: ~U[2020-01-01 00:00:00Z], end: ~U[2020-01-02 00:23:23Z]}
    s2 = %Stream{net: "RA", start: ~U[2020-01-01 12:00:00Z], end: ~U[2020-01-05 00:23:23Z]}
    assert Stream.intersection(s1, s2) == :empty
  end

  test "Intersect with inventory, simple case" do
    s1 = %Stream{net: "FR", sta: "CIEL", loc: "01", cha: "HNZ", start: ~U[2020-01-01 00:00:00Z], end: ~U[2020-01-02 00:23:23Z]}
    streams_list = Stream.match_with_inventory(s1)
    assert length(streams_list) == 1
    assert List.first(streams_list).start == s1.start
    assert List.first(streams_list).end == s1.end
  end

  test "Intersect with invertory splits the stream" do
    s1 = %Stream{net: "FR", sta: "WLS", loc: "00", cha: "HHN",
                 start: ~U[2021-08-05 00:00:00Z], end: ~U[2021-09-11 00:23:23Z]}
    streams_list = Stream.match_with_inventory(s1)
    assert length(streams_list) == 2
    [si1 , si2] = streams_list
    assert si1.start == s1.start
    assert si2.end == s1.end
  end
end
