defmodule Wsdataselect.ArchiveTest do
  require Temp
  require Logger
  use ExUnit.Case
  doctest Wsdataselect.Archive
  alias Wsdataselect.Archive

  test "File exists" do
    # Create a new file and test the function
    {:ok, tmp_path} = Temp.path
    archive = %Archive{path: tmp_path}
    refute Archive.exists?(archive)
    File.touch(tmp_path)
    assert Archive.exists?(archive)
  end

  test "Set start offset on typical usecase" do
    a = Archive.set_offset(
            %Archive{
                source_file: "PLOP",
                tsindex: Jason.decode!(
                "[{\"offset\": 0, \"timestamp\": 533174407868300000}, {\"offset\": 4096, \"timestamp\": 533175967868300000}, {\"offset\": 8192, \"timestamp\": 533185907861200000}, {\"offset\": 12288, \"timestamp\": 533196007861200000}, {\"offset\": 16384, \"timestamp\": 533206107861200000}, {\"offset\": 20480, \"timestamp\": 533216207861200000}, {\"offset\": 24576, \"timestamp\": 533226307861200000}, {\"offset\": 28672, \"timestamp\": 533236407861200000}, {\"offset\": 32768, \"timestamp\": 533246507861200000}, {\"offset\": 36864, \"timestamp\": 533256607861200000}]"
                )},
            ~U[1986-11-24 02:05:07Z], :before)
    Logger.info("Offset found at #{a.start_offset}")
    assert a.start_offset > 0
  end
  test "Set offset when start is after ts" do
    a = Archive.set_offset(
            %Archive{
                source_file: "PLOP",
                tsindex: Jason.decode!(
                "[{\"offset\": 0, \"timestamp\": 533174407868300000}, {\"offset\": 4096, \"timestamp\": 533175967868300000}, {\"offset\": 8192, \"timestamp\": 533185907861200000}, {\"offset\": 12288, \"timestamp\": 533196007861200000}, {\"offset\": 16384, \"timestamp\": 533206107861200000}, {\"offset\": 20480, \"timestamp\": 533216207861200000}, {\"offset\": 24576, \"timestamp\": 533226307861200000}, {\"offset\": 28672, \"timestamp\": 533236407861200000}, {\"offset\": 32768, \"timestamp\": 533246507861200000}, {\"offset\": 36864, \"timestamp\": 533256607861200000}]"
                )},
      ~U[1986-11-25 02:00:07Z], :before)
    assert a.start_offset == 36864
  end
  test "Set offset when start is before ts" do
    a = Archive.set_offset(
            %Archive{
                source_file: "PLOP",
                tsindex: Jason.decode!(
                "[{\"offset\": 0, \"timestamp\": 533174407868300000}, {\"offset\": 4096, \"timestamp\": 533175967868300000}, {\"offset\": 8192, \"timestamp\": 533185907861200000}, {\"offset\": 12288, \"timestamp\": 533196007861200000}, {\"offset\": 16384, \"timestamp\": 533206107861200000}, {\"offset\": 20480, \"timestamp\": 533216207861200000}, {\"offset\": 24576, \"timestamp\": 533226307861200000}, {\"offset\": 28672, \"timestamp\": 533236407861200000}, {\"offset\": 32768, \"timestamp\": 533246507861200000}, {\"offset\": 36864, \"timestamp\": 533256607861200000}]"
                )},
      ~U[1986-11-20 02:00:07Z], :before)
    assert a.start_offset == 0
  end

  test "Set end offset on typical usecase" do
    a = Archive.set_offset(
            %Archive{
                source_file: "PLOP",
                tsindex: Jason.decode!(
                "[{\"offset\": 0, \"timestamp\": 533174407868300000}, {\"offset\": 4096, \"timestamp\": 533175967868300000}, {\"offset\": 8192, \"timestamp\": 533185907861200000}, {\"offset\": 12288, \"timestamp\": 533196007861200000}, {\"offset\": 16384, \"timestamp\": 533206107861200000}, {\"offset\": 20480, \"timestamp\": 533216207861200000}, {\"offset\": 24576, \"timestamp\": 533226307861200000}, {\"offset\": 28672, \"timestamp\": 533236407861200000}, {\"offset\": 32768, \"timestamp\": 533246507861200000}, {\"offset\": 36864, \"timestamp\": 533256607861200000}]"
                )},
            ~U[1986-11-24 02:05:07Z], :after)
    Logger.info("Offset found at #{a.end_offset}")
    assert a.end_offset > 0
  end

  test "Set end offset when its later than the file" do
    a = Archive.set_offset(
            %Archive{
                source_file: "PLOP",
                tsindex: Jason.decode!(
                "[{\"offset\": 0, \"timestamp\": 533174407868300000}, {\"offset\": 4096, \"timestamp\": 533175967868300000}, {\"offset\": 8192, \"timestamp\": 533185907861200000}, {\"offset\": 12288, \"timestamp\": 533196007861200000}, {\"offset\": 16384, \"timestamp\": 533206107861200000}, {\"offset\": 20480, \"timestamp\": 533216207861200000}, {\"offset\": 24576, \"timestamp\": 533226307861200000}, {\"offset\": 28672, \"timestamp\": 533236407861200000}, {\"offset\": 32768, \"timestamp\": 533246507861200000}, {\"offset\": 36864, \"timestamp\": 533256607861200000}]"
                )},
            ~U[2000-11-24 02:05:07Z], :after)
    Logger.info("Offset found at #{a.end_offset}")
    assert a.end_offset == :nil
  end
end
