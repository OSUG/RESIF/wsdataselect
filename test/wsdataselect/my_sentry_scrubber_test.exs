defmodule Wsdataselect.MySentryScrubberTest do
  use ExUnit.Case, async: true
  use Plug.Test
  require Logger
  require MySentryScrubber

  test "Test scrubber for adding connection parameters" do
    test_conn = conn(:get, "/application.wadl")
    assert MySentryScrubber.scrub_params(test_conn) == %{aspect: :params}
    assert MySentryScrubber.scrub_params(%{test_conn | params: %{aspect: :params}}) == %{aspect: :params}

  end

end
