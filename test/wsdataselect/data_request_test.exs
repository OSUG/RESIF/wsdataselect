defmodule Wsdataselect.DataRequestTest do
  require Logger
  use ExUnit.Case
  alias Wsdataselect.{DataRequest, Stream}
  doctest Wsdataselect.DataRequest

  test "Intersect request with inventory" do
    d = %DataRequest{streams: [
                        %Stream{
                          net: "FR", sta: "WLS", loc: "00", cha: "HHN",
                          start: ~U[2021-08-05 00:00:00Z], end: ~U[2021-09-11 00:23:23Z]
                        }
                      ]
                    }
    di = DataRequest.match_streams_with_inventory(d)
    Logger.info(inspect(d))
    Logger.info(inspect(di))
    assert length(di.streams) == 2
  end
end
