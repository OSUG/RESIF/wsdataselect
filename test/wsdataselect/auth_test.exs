defmodule Wsdataselect.AuthTest do
  require Logger
  alias Wsdataselect.Auth
  use ExUnit.Case

  test "Get a proper challenge" do
    assert {:challenge_set, _} = Auth.authenticate(%Plug.Conn{})
  end

  test "Reject wrong password" do
    {:challenge_set, challenge} = Auth.authenticate(%Plug.Conn{})
    header = Auth.headers_to_map(challenge)
    Logger.notice(inspect(header))
    req_header = [{"authorization", "Digest username=resifdc, realm=FDSN, nonce=#{header.nonce}, opaque=#{header.opaque}, response=wrongresponse, uri=plop"}]
    assert {:error, m} = Auth.authenticate(%Plug.Conn{req_headers: req_header})
    Logger.notice(m)
  end
end
