defmodule Wsdataselect.BackendTest do
  alias Wsdataselect.{DataRequest, Backend, Stream, Archive}
  require DateTime
  require Logger
  use ExUnit.Case

  import ExUnit.CaptureLog
  doctest Backend

  test "Evaluate samples for small query" do
    {:ok, starttime, _} = DateTime.from_iso8601("2023-01-01T01:00:00Z")
    {:ok, endtime, _} = DateTime.from_iso8601("2023-01-01T02:00:00Z")

    datareq = %DataRequest{
      streams: [
        %Stream{net: "FR", sta: "CIEL", loc: "01", cha: "HNZ", start: starttime, end: endtime}
      ]
    }

    assert Backend.evaluate_samples(datareq) == datareq
  end

  test "Evaluate samples for multi parameters query" do
    {:ok, starttime, _} = DateTime.from_iso8601("2023-01-01T01:00:00Z")
    {:ok, endtime, _} = DateTime.from_iso8601("2023-01-01T02:00:00Z")

    datareq = %DataRequest{
      streams: [
        %Stream{net: "FR", sta: "CIEL", loc: "01", cha: "HNZ", start: starttime, end: endtime},
        %Stream{net: "FR", sta: "CIEL", loc: "01", cha: "HNE", start: starttime, end: endtime}
      ]
    }

    # assert Backend.evaluate_samples({:ok, datareq}) == {:ok, datareq}
    {result, logs} = with_log(fn -> Backend.evaluate_samples(datareq) end)
    assert result == datareq
    refute String.match?(logs, ~r"No sample for")
  end

  test "Get list of files in archive for simple request" do
    {:ok, starttime, _} = DateTime.from_iso8601("2023-01-01T01:00:00Z")
    {:ok, endtime, _} = DateTime.from_iso8601("2023-01-01T02:00:00Z")

    stream = %Stream{
      net: "FR",
      sta: "CIEL",
      loc: "01",
      cha: "HNZ",
      start: starttime,
      end: endtime
    }

    assert is_list(Backend.get_files(stream, "B"))
  end

  test "Extend temporary network code" do
    assert Backend.extend_code("FR", 1900) == "FR"
    assert Backend.extend_code("XP", 2024) == "XP2023"
  end

  test "Stream allowed for anonymous ?" do
    assert Backend.stream_allowed?(%Stream{net_id: 42}, "anonymous")
    assert Backend.stream_allowed?(%Stream{net_id: 42}, "anonymous")
    refute Backend.stream_allowed?(%Stream{net_id: 71}, "anonymous")
  end


  test "make_selection_input/1" do
    {:ok, starttime, _} = DateTime.from_iso8601("2023-01-01T01:00:00Z")
    {:ok, endtime, _} = DateTime.from_iso8601("2023-01-01T02:00:00Z")

    datareq = %DataRequest{
      streams: [
        %Stream{net: "FR", sta: "CIEL", loc: "01", cha: "HNZ", start: starttime, end: endtime}
      ]
    }

    results = Wsdataselect.Backend.make_selection_input(datareq)
    assert results =~ "FR CIEL 01 HNZ * 2023,001,01,00,00.000000 2023,001,02,00,00.000000"
  end

  # Ces fonctions sont mainenant privées, on ne les teste plus
  # test "Keep files of best quality" do
  #   files = [
  #     %{source_file: "a", quality: "R"},
  #     %{source_file: "a", quality: "Q"},
  #     %{source_file: "a", quality: "M"},
  #     %{source_file: "a", quality: "undefined"},
  #     %{source_file: "a", quality: "D"}
  #     ]
  #   assert Backend.keep_best_quality(files) == %{source_file: "a", quality: "Q"}
  # end

  # test "Filter on quality" do
  #   files = [
  #     %{source_file: "a", quality: "R"},
  #     %{source_file: "a", quality: "Q"},
  #     %{source_file: "b", quality: "M"},
  #     %{source_file: "b", quality: "undefined"},
  #     %{source_file: "b", quality: "D"}
  #     ]
  #   assert Backend.filter_on_quality(files) == [
  #     %{source_file: "a", quality: "Q"},
  #     %{source_file: "b", quality: "M"}
  #   ]
  # end
end
