defmodule Wsdataselect.MixProject do
  use Mix.Project

  def project do
    [
      app: :wsdataselect,
      version: "1.0.4",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: [
        extras: ["README.md"],
        source_url: "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/wsdataselect"
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Wsdataselect.Application, []}
    ]
  end

  defp deps do
    [
      {:bandit, "~> 1.0"},
      {:ecto_sql, "~> 3.0"},
      {:pg_ranges, "~> 1.1.1"},
      {:postgrex, ">= 0.0.0"},
      {:timex, "~> 3.0"},
      # tzdata is a dep of timex, this release has an important fix, timex needs to bump to > 3.7.11
      # {:tzdata, "~> 1.1.2"},
      {:redix, "~> 1.2"},
      {:jason, "~> 1.4"},
      {:cachex, "~> 3.6"},
      {:ex_doc, "~> 0.30", only: :dev, runtime: false},
      {:dialyxir, "~> 1.4", only: [:dev], runtime: false},
      {:credo, "~> 1.7", only: [:dev, :test], runtime: false},
      {:temp, "~> 0.4", only: [:test], runtime: false},
      {:eflambe, "~> 0.3", only: :dev},
      {:meck, "~>0.9.2", only: :dev},
      {:sentry, "~> 10.0"},
      {:bisect, "~> 0.4.0"}
    ]
  end
end
