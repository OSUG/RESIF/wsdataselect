ARG ELIXIR_VERSION=1.17.3
ARG OTP_VERSION=27.1.2
ARG DEBIAN_VERSION=bookworm-20241111-slim
ARG BUILDER_IMAGE="docker.io/hexpm/elixir:${ELIXIR_VERSION}-erlang-${OTP_VERSION}-debian-${DEBIAN_VERSION}"
ARG RUNNER_IMAGE="docker.io/debian:${DEBIAN_VERSION}"

FROM ${BUILDER_IMAGE} as builder
WORKDIR /src
RUN apt-get update \
  && apt-get install -y --no-install-recommends build-essential curl inotify-tools wget  ca-certificates \
  && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
  && apt-get clean
ARG DATASELECT_VERSION=4.0.1
RUN echo ${DATASELECT_VERSION}
RUN wget https://github.com/EarthScope/dataselect/archive/refs/tags/v${DATASELECT_VERSION}.tar.gz
RUN tar xf v${DATASELECT_VERSION}.tar.gz
WORKDIR /src/dataselect-${DATASELECT_VERSION}
RUN make
WORKDIR /app
COPY mix.exs .
COPY mix.lock .
RUN mix deps.get
RUN mix deps.compile
WORKDIR /app
COPY config ./config
COPY priv ./priv
COPY lib ./lib
ARG MIX_ENV=dev
RUN MIX_ENV=$MIX_ENV mix sentry.package_source_code
RUN MIX_ENV=$MIX_ENV mix release

FROM ${RUNNER_IMAGE}
RUN apt-get update \
  && apt-get install -y --no-install-recommends libssl3 \
  && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
  && apt-get clean
WORKDIR /app
ARG DATASELECT_VERSION=4.0.1
COPY --from=builder /src/dataselect-${DATASELECT_VERSION}/dataselect /bin/dataselect
ARG MIX_ENV=dev
COPY --from=builder /app/_build/$MIX_ENV/rel/ .
RUN chown -R 1500 wsdataselect
USER 1500
# Setting sentry release at end of Docker build in order do avoid repetition
ARG SENTRY_RELEASE
ENV SENTRY_RELEASE=${APP_REVISION:-none}
ARG APP_REVISION
ENV APP_REVISION=${APP_REVISION:-main}
#
CMD ["wsdataselect/bin/wsdataselect", "start"]
